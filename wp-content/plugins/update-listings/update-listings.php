<?php
/*
Plugin Name: Update Listings
Description: Update associate and practice listings
Version: 1.0
*/

function update_page( $post_id = 0, $updatedContent ) {
        
        if( $updatedContent != '' ){
        
            if( $post_id == 2696 ){
            
                // We unhook this action to prevent an infinite loop
        		remove_action( 'save_post', 'update_page' );
        
        		$args = array(
        			'ID' => $post_id,
        			'post_content' => $updatedContent
        		);
        		$updateID = wp_update_post( $args );
        		// Now hook the action
        		add_action( 'save_post', 'update_page' );
        	    return $updateID;            
            }   
        }
}

add_action( 'save_post', 'update_page' );

function filter_associate_listings(){
  
    $myform = "<form action='' method='get' id='filter-state-form'>";
    $myform .= "<label for='associate-filter'>Filter State: </label>";
    $myform .= "<select name='associate-listing-state' id='associate-filter' required>";
    $myform .= "<option>--Select a state--</option>";
    $states = array( 
		"AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC",  
    "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA",  
    "MA", "MD", "ME", "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE",  
    "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI", "SC",  
    "SD", "TN", "TX", "UT", "VA", "VT", "WA", "WI", "WV", "WY");
    
    foreach( $states as $state ){
        
        $myform .= "<option value='".strtolower($state)."'>".$state."</option>";
    }

    $myform .= "</select><button type='submit' style='margin:5px'>Filter</button>";
    
    //Reset glyphicon <span class='dashicons dashicons-image-rotate'></span>
    
    $myform .= "<a href='https://hemmenassoc.com/all-associate-listings' style='margin-top: 10px; margin-left: 5px;'>Reset</a></form>";
    return $myform;
    
}

add_shortcode( 'filter-associate-listings', 'filter_associate_listings' );

function update_listings_admin_menu_option(){
    
    add_menu_page( 'Add listings', 'Add Listings', 'manage_options', 'add-listings', 'add_listings_page', '', 200 );
    
}

add_action( 'admin_menu', 'update_listings_admin_menu_option' );

function add_listings_page(){
    
    if( $_POST['select_state'] ){
        
        // $postdata = "<div  class='postData' style='display:none'>";
        // $postdata .= "<p  id='listingID'>".$_POST['listing_id']."</p>";
        // $postdata .= "<p  id='listingState' class='".$_POST['select_state']."' >".$_POST['select_state']."</p>";
        // $postdata .= "<p  id='listingAvailability'>".$_POST['availability']."</p>";
        // $postdata .= "<p  id='listingLocation'>".$_POST['location']."</p>";
        // $postdata .= "<p id='listingDesc' >".$_POST['description']."</p>";
        // $postdata .= "</div>";
        //echo $postdata;
        
        //Getting post content for all associate listings page
        $my_id = 2696;
        $post_id_5369 = get_post($my_id);
        $content = $post_id_5369->post_content;
        
        
        //$content = apply_filters('the_content', $content);
        //$content = str_replace(']]>', ']]>', $content);
        //echo  '<div id="contentFromTheFunction" data-type="'.gettype($content).'">'. $content . '</div>';
        
        
        $str_to_insert = '<tr class="'.$_POST['select_state'].' associate-listing-details">
				<td width="20%"><b>Listing ID#:</b> #'.$_POST['listing_id'].'</td>
				<td width="10%"><b>State:</b> '.strtoupper($_POST['select_state']).'</td>
				<td width="25%"><b>Status:</b> '.$_POST['availability'].'</td>
				<td width="45%"><b>Location:</b> '.$_POST['location'].'</td>
			</tr>
			<tr class="'.$_POST['select_state'].' associate-listing-description">
				<td colspan="4"><b>Description:</b>
				'.$_POST['description'].'</td>
			</tr>
			<tr> <!--line break-->
				<td colspan="4">

				<hr />

				<hr />

				</td>
			</tr>';
        //echo $newstr = substr_replace($oldstr, $str_to_insert, 12, 0);
        
        $newstr = substr_replace($content, $str_to_insert, 105, 0);
        //echo '<div id="updateHtml">'.$newstr.'</div>';
        
        
        $mypostId = 2696;
        $updateList = update_page( $mypostId, $newstr );
        if( $updateList == 2696 ){
            $notice = "<div class='updated notice'>
                        <p cstyle='color:green; font-weight:bold;'>Your listing has been added!</p>
                  </div>";
            echo $notice;
        }else{
            $notice = "<div class='notice notice-error is-dismissible'>
                        <p cstyle='color:red; font-weight:bold;'>Your listing has not been added.</p>
                  </div>";
            echo $notice;
        }
    }
    
    $states = array( 
		"AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC",  
    "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA",  
    "MA", "MD", "ME", "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE",  
    "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI", "SC",  
    "SD", "TN", "TX", "UT", "VA", "VT", "WA", "WI", "WV", "WY"); 
    
    ?>
    <h1>Add associate listing</h1>
    <form action="" method="POST">
        
        <p><label for="listingID">Listing ID</label><br>
        <input type="text" name="listing_id" id="listingID" required></p>
        <p><label for="selectState">State</label><br>
        <select name="select_state" id="selectState" required>
            <option>--Select state--</option>
            <!--<option value="ia">IA</option>-->
            <!--<option value="il">IL</option>-->
            <!--<option value="ne">NE</option>-->
            <!--<option value="wi">WI</option>-->
            
            <?php
            foreach( $states as $state ){
                
                echo "<option value='".strtolower($state)."'>".$state."</option>";
                
            }
            ?>
            
        </select></p>
        <p><label for="availablility">Availability</label><br>
        <input type="radio" name="availability" value="available" checked> Available<br>
        <input type="radio" name="availability" value="not available"> Not Available<br></p>
        <p><label for="listing_location">Location</label><br>
        <input type="text" name="location" id="listing_location" required></p>
        <p><label for="listing_description" required>Description</label><br>
        <textarea name="description" id="listing_description" rows="4" cols="50"></textarea></p>
        <button type='submit' name='submit_listing'>Add</button>
    </form>    
        
    <?php
    
}