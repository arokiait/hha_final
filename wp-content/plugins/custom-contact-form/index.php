<?php
/*
Plugin Name: Custom Contact Form Plugin
 
Description: Simple non-bloated WordPress Contact Form
Version: 1.0
Author: Darshan
 
*/
 
 require_once dirname(__FILE__) . "/sendgrid-php/sendgrid-php.php"; 
function html_form_code() {
	echo '<form action="' . esc_url( $_SERVER['REQUEST_URI'] ) . '" method="post">';
 ?>
<div class="gform_body">
	<div class="form-page-right" style="width:100%">
		<div role="form" class="wpcf7" id="wpcf7-f920-p18-o1" dir="ltr" lang="en-US">
			<div class="screen-reader-response"></div>
			<form action="#" method="post" class="wpcf7-form" >
				<p>
					<label> Your Name *
						<br>
							<span class="wpcf7-form-control-wrap your-name">
								<input type="text" name="your-name" required  value="<?php if (isset($_POST['your-name'])) echo $_POST['your-name']; ?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
								</span>
							</label>
						</p>
						<p>
							<label> Your Email *
								<br>
									<span class="wpcf7-form-control-wrap your-email">
										<input type="email" required name="your-email" value="<?php if (isset($_POST['your-email'])) echo $_POST['your-email']; ?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false">
										</span>
									</label>
								</p>
								<p>
									<label>Your Phone Number *
										<br>
											<span class="wpcf7-form-control-wrap tel-91">
												<input type="tel" required name="numbertel" value="<?php if (isset($_POST['numbertel'])) echo $_POST['numbertel']; ?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false">
												</span>
											</label>
										</p>
										<p>
											<label>Questions/Comments
												<br>
													<span class="wpcf7-form-control-wrap textarea-579">
														<textarea required name="question" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"><?php if(isset($_POST['question'])) echo $_POST['question']; ?></textarea>
													</span>
												</label>
											</p>
											<p></p>
											<div class="g-recaptcha" data-sitekey="6LeAWP8UAAAAAOyzMXBkolrXmxd_EvzCy0vgL3A7"></div>
											<p>
												<input type="submit" value="Submit" name="form_sub" class="wpcf7-form-control wpcf7-submit">
													<span class="ajax-loader"></span>
												</p>
											</form>
										</div>
									</div>
								</div>			  
	 				 
 <?php
	echo '</form>';
}



function deliver_mail() {

	// if the submit button is clicked, send the email
	$returnMsg = ''; 
	if ( isset( $_POST['form_sub'] ) )
	{ 
	      if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
	        { 
                // Google reCAPTCHA API secret key 
                $secret_key = '6LeAWP8UAAAAAFrvcd55G2RwSK8R-4Wl26eKy5v7';  
                 
                // reCAPTCHA response verification
                $verify_captcha = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$_POST['g-recaptcha-response']); 
                 
                // Decode reCAPTCHA response 
                $verify_response = json_decode($verify_captcha); 
                 
                // Check if reCAPTCHA response returns success 
                if($verify_response->success)
                { 
                    //echo "<script>alert('DOne..')</script>";
                    
                    $name = $_POST['your-name']; 
 
                    $email = $_POST['your-email']; 
                    $phone = $_POST['numbertel'];
    				$question = $_POST['question'];
                    $credare=date("Y-m-d");
                    global $wpdb;
                    $wpdb->insert('wp_own_contact_form', array(
                                'first_name' => $name,
                                   'email' => $email,
                                'phone' => $phone,
                                'message'  => $question,
                                'created_date'  => $credare,
                               )
                     );
                     $subject="Appraisal Request";
                	$content = new SendGrid\Content('text/plain','
                		Name: '.$name.'
                	 	Email: '.$email.'
                		Phone: '.$phone.'
                		Message: '.$question.'');
                    $headers = "From: noreplay@hemmenassoc.com" . "\r\n";
                    $emailnew="info@hemmenassoc.com";
                    $value=$question;
                     $from = new SendGrid\Email("Appraisal Request", $emailnew);
                     $apiKey = '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';
                    
                   //  $badWords1 = file_get_contents('http://devtest.arokiait.com/wp-content/plugins/custom-contact-form/offensive.txt');
                   
                          $badWords1 = file_get_contents('https://hemmenassoc.com/wp-content/plugins/custom-contact-form/offensive.txt');
                    if(preg_match("/$badWords1/", $value) === 1) 
                    {
                     //if(preg_match('(bad|naughty|fuck)', $value) === 1) 
                      ///////////////////////////////email to if msg contains Offensive words//////////////////////////////////////////
                          $to = new SendGrid\Email("Darshan", "darshanjain.arokia@gmail.com");
                        	$mail = new SendGrid\Mail($from, $subject, $to, $content);
         	                 $sg = new \SendGrid($apiKey);
                        	$response = $sg->client->mail()->send()->post($mail);
                        	$emailstatus=$response->statusCode();
                        	if($emailstatus=="202")
                       // if ( wp_mail( $to, $subject, $message, $headers ) ) 
                        {
                      	     echo "<script>
                                alert('Spam detected. Your message has not been sent. Please try again');
                                window.location.href='/';
                                </script>";
                    	}
                    	else 
                    	{
    			            echo '<p style="color:red;"> An unexpected error occurred</p>';
    		            }
                 
                    }
                    else if(!preg_match('/^[a-z0-9 .}, !@#$%^&*()_+|\';?><-]+$/i', $value))
                    {
                         // detects link & non english text
                         ///////////////////////////////email to if msg contains link & non english//////////////////////////////////////////
                          $to = new SendGrid\Email("Darshan", "darshanjain.arokia@gmail.com");
                        	$mail = new SendGrid\Mail($from, $subject, $to, $content);
         	                 $sg = new \SendGrid($apiKey);
                        	$response = $sg->client->mail()->send()->post($mail);
                        	$emailstatus=$response->statusCode();
                        	if($emailstatus=="202")
                       // if ( wp_mail( $to, $subject, $message, $headers ) ) 
                         {
                      	     echo "<script>
                                 alert('Spam detected. Your message has not been sent.Please try again');
                                window.location.href='/';
                                </script>";
                    	} 
                    	else 
                    	{
    		            	echo '<p style="color:red;"> An unexpected error occurred</p>';
    		            }
                    }
                    else if(preg_match('%^((https?://)|(www\.))([a-z0-9-].?)+(:[0-9]+)?(/.*)?$%i', $value))
                    {
                        //only link
                        ///////////////////////////////email to if msg contains links//////////////////////////////////////////
                             $to = new SendGrid\Email("Darshan", "darshanjain.arokia@gmail.com");
                        	$mail = new SendGrid\Mail($from, $subject, $to, $content);
         	                 $sg = new \SendGrid($apiKey);
                        	$response = $sg->client->mail()->send()->post($mail);
                        	$emailstatus=$response->statusCode();
                        	if($emailstatus=="202")
                           {
                      	     echo "<script>
                                alert('Spam detected. Your message has not been sent.Please try again');
                                window.location.href='/';
                                </script>";
            		    } 
            		    else 
            		    {
    			            echo '<p style="color:red;"> An unexpected error occurred</p>';
    		            }
                    }
                    else
                    {
                        ///////////////////////////////email to if msg looks good//////////////////////////////////////////
                        
                             $to = new SendGrid\Email("Henry Hemmen & Associates", "tom@hemmenassoc.com");
                        	$mail = new SendGrid\Mail($from, $subject, $to, $content);
         	                 $sg = new \SendGrid($apiKey);
                        	$response = $sg->client->mail()->send()->post($mail);
                        	$emailstatus=$response->statusCode();
                        	if($emailstatus=="202")
                            {
                      	     echo "<script>
                                alert('Thanks for contacting Henry Hemmen & Associates. Your message has been sent and you can expect a response soon.');
                                window.location.href='/';
                                </script>";
            		   	} 
            		   	else 
    		            {
    			            echo '<p style="color:red;"> An unexpected error occurred</p>';
    		            }
                    } 	 
                                 
                    //$returnMsg = 'Your registration has submitted successfully.'; 
                }
                else
                { 
                    $returnMsg = '<p style="color:red">reCaptch verification failed, please verify again.</p>'; 
                } 
            }
            else
            { 
                $returnMsg = '<p style="color:red">Please check the CAPTCHA box.</p>'; 
            } 
     
    } 
    echo $returnMsg;
}
 

function cf_shortcode() {
	ob_start();
	deliver_mail();
	html_form_code();

	return ob_get_clean();
}

add_shortcode( 'sitepoint_contact_form', 'cf_shortcode' );

function installer(){
    include('installer.php');
}
register_activation_hook( __file__, 'installer' );

add_action( 'admin_menu', 'register_my_custom_menu_page' );

function register_my_custom_menu_page(){
    add_menu_page( 'my plugin', 'Custom Plugin', 'manage_options', 'my-plugin-settings', 'my_plugin_custom_function', plugins_url( ' ' ), 66 );
}
 