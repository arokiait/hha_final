<?php
/**
Plugin Name: Practice Alerts Email
Description: This is a custom plugin to sign up for practice alerts 
*/

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit; 
}

// Generate the Captcha Equation and Save the Result
    
session_start();
$salt = 'dfsjhgf564gf5623';

function pa_subscribe_form(){
    

    if( array_key_exists('submit', $_POST) ){
        
      
        $message = '';
        
        if ( empty( $_POST['user_email'] )  ) {
            
            $message = 'Please fill out your email address';
    
        }
        else{
            
          $email    =  $_POST["user_email"];
          $fromMail = "admin@hemmenassoc.com";
          $headers  = 'From:' . $fromMail . "\r\n";
          $to = "info@hemmenassoc.com";
// $to = "darshanjain.arokia@gmail.com";
         
          $subject = 'New sign up - Practice alerts';
          $message = 'The following user has requested to sign up for Practice Alerts: '.$email;
          
          $sent = wp_mail( $to, $subject, $message, $headers );
          if( $sent ){
              
              $message = '<p class="practice-alerts-msg" id="practice-alerts-success" style="color:green">Sign up successful!</p>';
            
          }else{
              
              $message = '<p class="practice-alerts-msg" id="practice-alerts-error" style="color:red">Failed to sign you up. Please try again later.</p>';
              
          }
          
          /* Creating a db record */
          global $wpdb;
          $date = date('Y-m-d H:i:s');
          $table = $wpdb->prefix.'email_list_practice_alerts';
          $data = array('email'=> $email, 
                        'sign_up_date' => $date);
          $format = array('%s','%s');
          $wpdb->insert( $table, $data, $format );
          $my_id = $wpdb->insert_id;
          if( $my_id != 0 ){
              //echo 'Insert id: ' . $my_id ;
          }
        }
          
    }
    
    
   
    $status_msg = $message;
    
    $form = "<h1 id='pa_heading'>Sign up for Practice Alerts</h1>";
    $form .= "<form action='' method='post'>";
    $form .= "<div id='statusMessage'>".$status_msg."</div>";
    $form .= "<p><input type='email' name='user_email' id='practice-alerts-email' placeholder='Enter your email here' style='padding:10px' required='required'></p>";
 
    $form .= "<input type='submit' name='submit' id='practice-alerts-signup' value='Sign Up' >";
    $form .= "</form>"; 
    
    return $form; 
}

add_shortcode('show_pa_form','pa_subscribe_form');
