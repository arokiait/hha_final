<!-- start Simple Custom CSS and JS -->
<script type="text/javascript">
jQuery(document).ready(function( $ ){
   
  /* Scroll down to status message */
  if( $('.practice-alerts-msg').length > 0 ){
     
  	$([document.documentElement, document.body]).animate({
        scrollTop: $(".practice-alerts-msg").offset().top
    }, 2000);
     
  }
  

  

  
  /* Function to get url parameter */
  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    	for (i = 0; i < sURLVariables.length; i++) {
          sParameterName = sURLVariables[i].split('=');

          if (sParameterName[0] === sParam) {
              return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
              }
    	}
	};
  
  /* Chek current page */
  var pathname = window.location.pathname; 
  if( pathname == '/all-associate-listings/' ){
  		
      var selected_state = getUrlParameter('associate-listing-state');
      
      if( typeof selected_state !== 'undefined' ){
      		//alert('asdf');
      		 $('.associate-listing-details').each(function(){
                  if( !$(this).hasClass(selected_state) ){
                      $(this).hide();
                  }
            });
            $('.associate-listing-description').each(function(){
                    if( !$(this).hasClass(selected_state) ){
                        $(this).hide();
                        $(this).next().hide();
                    }
              });	
        
      }
    if( $('.postData').length > 0 ){
    	var l_id = $('#listingID').val();
    	var listState = $('#listingState').val();
    	var availability = $('#listingAvailability').val();
     	var location = $('#listingLocation').val();
    	var desc = $('#listingDesc').val(); 
    }
     
    
   
    
    
  }
  
});
</script>
<!-- end Simple Custom CSS and JS -->
