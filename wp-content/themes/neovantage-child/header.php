<?php

/**

 * The header for our theme.

 *

 * This is the template that displays all of the <head> section and everything up until <div id="content">

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package NEOVANTAGE

 */

?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

    <head>

        <meta charset="<?php bloginfo('charset'); ?>">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="profile" href="http://gmpg.org/xfn/11">

        <?php wp_head(); ?>

            <style>
                @media all and (-ms-high-contrast:none)
                {
                    .since h1::before, .home-content h2::before, #custom_html-3 h1::before { width: 170px; } /* IE10 */
                    *::-ms-backdrop, .since h1::before, .home-content h2::before, #custom_html-3 h1::before { width: 170px; } /* IE11 */
                }
            </style>
            
            
            
    </head>

    <body <?php body_class(); ?>>

    <?php

    $neovantage_code_body_start = get_theme_mod('_neovantage_code_body_start','');

    if( '' != $neovantage_code_body_start) :

        echo $neovantage_code_body_start;

    endif;

    ?>

    <div id="page" class="site">

        <?php /* Preloader */ neovantage_render_preloader(); ?>

        <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'neovantage'); ?></a>

        <?php if( !is_page_template( 'page-templates/template-landing.php' ) ): ?>





<!--<div>

<h1 style="text-align:center;font-size: 24px;line-height: 2.5; margin-bottom: 0px;">Site Under Maintenance</h1>

<p style="text-align:center;font-size: 16px;line-height: 1.5;">For Enquiries Contact</p>

<p style="text-align:center;font-size: 16px;line-height: 1.5;">Phone: (800) 745-1438</p>

<p style="text-align:center;font-size: 16px;line-height: 1.5;">info@hemmenassoc.com</p>

</div>-->

        

        <header id="masthead" class="site-header" role="banner"

        <?php if ( get_header_image() ) : ?>

        style="background-image: url(<?php header_image(); ?>);"

        <?php endif; ?>

        >

            <div class="container">

                <div class="row">

                    <div class="col-lg-3 col-xs-3">

                        <div class="site-branding">

                            <div class="site-branding-position">

                                <?php

                                $custom_logo_id = get_theme_mod( 'custom_logo' );

                                if( isset( $custom_logo_id ) && !empty( $custom_logo_id ) ) {

                                    neovantage_site_logo();

                                } else {

                                ?>

                                <?php if (is_front_page() && is_home()) : ?>

                                <h1 class="site-title"><a href="<?php echo esc_url( home_url('/') ); ?>" rel="home"><span><?php echo get_bloginfo('name'); ?></span></a></h1>

                                <?php else : ?>

                                <p class="site-title"><a href="<?php echo esc_url( home_url('/') ); ?>" rel="home"><span><?php echo get_bloginfo('name'); ?></span></a></p>

                                <?php endif; ?>

                                <?php }?>

                            </div>

                        </div>

                    </div>

                    <div class="col-lg-9 col-xs-9">

                        <nav class="neovantage-main-menu" role="navigation">

                            <?php

                            if (has_nav_menu('primary')) {

                                wp_nav_menu(

                                    array(

                                        'menu'              => 'primary',

                                        'theme_location'    => 'primary',

                                        'depth'             => 3,

                                        'container'         => FALSE,

                                        'menu_class'        => 'nav main-nav hidden-sm hidden-xs',

                                        'walker'            => new Neovantage_Walker_Nav_Menu_Main()

                                    )

                                );

                            } else {

                                echo '<span class="no-nav">'. __('No custom menu created!', 'neovantage') .'</span>';

                            }

                            ?>

                            <div class="toggle-bar">

                                <a href="javascript:void(0);"><i class="fas fa-bars"></i></a>

                            </div>

                        </nav><!-- #site-navigation -->

                        <nav class="neovantage-mobile-menu" role="navigation">

                            <a href="javascript:void(0);" class="neovantage-mobile-menu-close"><i class="fas fa-times"></i></a>

                            <?php

                            if (has_nav_menu('mobile')) {

                                wp_nav_menu(

                                    array(

                                        'menu'              => 'mobile',

                                        'theme_location'    => 'mobile',

                                        'depth'             => 3,

                                        'container'         => FALSE,

                                        'menu_class'        => 'nav mobile-nav',

                                        'walker'            => new Neovantage_Walker_Nav_Menu_Mobile()

                                    )

                                );

                            } else {

                                echo '<span class="no-nav">'. __('No custom menu created!', 'neovantage') .'</span>';

                            }

                            ?>

                        </nav>

                    </div>

                </div>

            </div>

        </header><!-- #masthead -->

        <?php endif; ?>



        <?php if( !is_page_template( 'page-templates/template-slideshow.php' ) && !is_page_template( 'page-templates/template-landing.php' ) && !is_page_template( 'page-templates/template-fullwidth-with-no-page-header.php' ) ): ?>

        

        <!-- Breadcrumb -->

        <!--<section class="page-header">

            <div class="container">

                <div class="row">

                    <div class="col-md-6">

                        <?php if( is_home() && get_option('page_for_posts') ) { ?>

                        <h2><?php echo apply_filters('the_title', get_page( get_option('page_for_posts') )->post_title); ?></h2>

                        <?php } elseif(is_archive() ) {

                        the_archive_title( '<h2 class="page-title"><span>', '</h2></span>' );

                        

                        if(!is_author()) :

                        the_archive_description( '<div class="taxonomy-description">', '</div>' );

                        endif;

                        

                        }  elseif(is_search() ) { ?>

                        <h2 class="page-title"><?php printf(esc_html__('Search Results for: %s', 'neovantage'), '<span>' . get_search_query() . '</span>'); ?></h2>

                        <?php } elseif( is_singular() ) { ?>

                        <h2><?php the_title(); ?></h2>

                        <?php } ?>

                    </div>

                    <div class="col-md-6"><?php echo neovantage_breadcrumbs(); ?></div>

                </div>

            </div>

        </section> -->
                            
        <?php endif; ?>
        <section id="banner-section">
            <div class="container">
                <div class="col-xs-12">
                    <?php if(is_front_page() ) { ?>  <?php echo do_shortcode("[sp_responsiveslider]"); ?> <?php } ?>
                </div>
            </div>
        </section>

        <section id="content" class="site-content">
                            