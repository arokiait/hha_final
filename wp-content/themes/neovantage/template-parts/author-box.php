<?php
/**
 * Template part for displaying author infor.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package NEOVANTAGE
 */
$author_id = get_the_author_meta('ID');
?>
<div class="author-box">
    <div class="author-img">
        <?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'neovantage_author_bio_avatar_size', 100 ) ); ?>
    </div>
    <div class="author-content">
        <div class="author-url">
            <?php if( get_the_author_meta('user_url') ) : ?>
            <a href="<?php the_author_meta('user_url'); ?>" target="_blank"><?php the_author_meta('user_url'); ?></a> <?php _e('-', 'neovantage'); ?>
            <?php endif; ?>
            <span><?php echo ( count_user_posts( $author_id ) > 1 ) ? sprintf( __( '%d Posts', 'neovantage' ), count_user_posts( $author_id ) ) : sprintf( __( '%d Post', 'neovantage' ), count_user_posts( $author_id ) ); ?></span>
        </div>
        <?php if ( get_the_author_meta( 'description' ) ) : ?>
        <div class="author-description"><?php the_author_meta( 'description' ); ?></div>
        <?php endif; ?>
        <?php if( get_the_author_meta('author_facebook') ) : ?>
        <a target="_blank" class="author-social" href="<?php the_author_meta('author_facebook'); ?>"><i class="fab fa-facebook"></i></a>
        <?php endif; ?>
        <?php if( get_the_author_meta('author_twitter') ) : ?>
        <a target="_blank" class="author-social" href="<?php the_author_meta('author_twitter'); ?>"><i class="fab fa-twitter"></i></a>
        <?php endif; ?>
        <?php if( get_the_author_meta('author_gplus') ) : ?>
        <a target="_blank" class="author-social" href="<?php the_author_meta('author_gplus'); ?>"><i class="fab fa-google-plus"></i></a>
        <?php endif; ?>
        <?php if( get_the_author_meta('author_linkedin') ) : ?>
        <a target="_blank" class="author-social" href="<?php the_author_meta('author_linkedin'); ?>"><i class="fab fa-linkedin"></i></a>
        <?php endif; ?>
        <?php if( get_the_author_meta('author_dribbble') ) : ?>
        <a target="_blank" class="author-social" href="<?php the_author_meta('author_dribbble'); ?>"><i class="fab fa-dribbble"></i></a>
        <?php endif; ?>
    </div>
</div>