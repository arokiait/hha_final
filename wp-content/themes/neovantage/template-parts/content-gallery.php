<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package NEOVANTAGE
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    
    <!-- .entry-slider -->
    <div class="entry-slider">
        <?php
        $gallery_ids = neovantage_grab_ids_from_gallery()->ids;
        $gallery_columns = neovantage_grab_ids_from_gallery()->columns;
        
        if ( ! empty( $gallery_ids ) && $gallery_columns == '1' ):
            $i = 0;
        ?>
        <div id="post-slider" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <?php
                foreach ( $gallery_ids as $image_id ): ?>
                <?php
                $attachment_image = wp_get_attachment_image_src( $image_id, 'large', FALSE );
                if ( $attachment_image[ 0 ] != '' ):
                    $class_active = ( $i == 0 ) ? ' active' : '';
                ?>
                <div class="item <?php echo $class_active; ?>"><img alt="" src="<?php echo esc_url( $attachment_image[ 0 ] ); ?>"></div>
                <?php
                $i++;
                endif;
                ?>
                <?php endforeach; ?>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#post-slider" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only"><?php echo __('Previous', 'neovantage'); ?></span>
            </a>
            <a class="right carousel-control" href="#post-slider" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only"><?php echo __('Next', 'neovantage'); ?></span>
            </a>
        </div>
        <?php endif; ?>
    </div>
    
    <!-- .entry-header -->
    <?php get_template_part( 'template-parts/content', 'header' ); ?>
    
    <!-- .entry-content -->
    <div class="entry-content">
        <?php if ( ! is_single() ) : // Only display Excerpts for Search ?>
            <?php the_excerpt(); ?>
        <?php else : ?>
            <?php
            the_content(
                sprintf(
                    wp_kses(
                        /* translators: %s: Name of current post. Only visible to screen readers */
                        __('Continue reading<span class="screen-reader-text"> "%s"</span>', 'neovantage'),
                        array(
                            'span' => array(
                                'class' => array(),
                            ),
                        )
                    ),
                    get_the_title()
                )
            );
            wp_link_pages(
                array(
                    'before' => '<div class="page-links">' . esc_html__('Pages:', 'neovantage'),
                    'after' => '</div>',
                )
            );
            ?>
        <?php endif; ?>
        <?php neovantage_entry_footer(); ?>
    </div><!-- .entry-content -->
    <?php if ( 'post' === get_post_type() ) : ?>
    <div class="entry-meta clearfix">
        <?php neovantage_entry_meta(); ?>
        <?php if (!is_single()) : // Only display Excerpts for Search ?>
        <a href="<?php the_permalink(); ?>" rel="bookmark">
            <?php
            printf(
                /* translators: %s: Name of current post. */
                wp_kses( __('Continue reading %s <span class="meta-nav">&rarr;</span>', 'neovantage'),
                array('span' => array('class' => array())) ), the_title('<span class="screen-reader-text">"', '"</span>', false)
            )
            ?>
        </a>
        <?php endif; ?>
    </div><!-- .entry-meta -->
    <?php endif; ?>
</article><!-- #post-## -->