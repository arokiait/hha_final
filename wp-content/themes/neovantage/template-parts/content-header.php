<?php if ( !is_single() ) { ?>
<header class="entry-header">
    <?php if("link" == get_post_format()) : ?>
        <?php $link_url = get_post_meta(get_the_ID(), '_neovantage_link_url', true); ?>
        <?php
        $link_title = sprintf(
            /* translators: %s: link post title. */
            esc_html_x( '%s', 'link post title', 'neovantage' ),
            '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $link_url . '</a></h2>'
        );
        echo $link_title;
        ?>
    <?php else: ?>
        <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
    <?php endif; ?>
    
    
</header><!-- .entry-header -->
<?php }