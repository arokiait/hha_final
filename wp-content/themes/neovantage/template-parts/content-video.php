<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package NEOVANTAGE
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <!-- Edit Article -->
    <?php neovantage_edit_post(); ?>
    
    <!-- entry-media -->
    <?php
    $video_url = get_post_meta( (int) get_the_ID(), '_neovantage_video_url', TRUE );
    if ($video_url):
    ?>
    <div class="entry-media clearfix">
        <?php echo do_shortcode('[video src="'.esc_url( $video_url ).'" width="1920" height="1080"][/video]'); ?>
    </div>
    <?php endif; ?>
    
    <!-- .entry-header -->
    <?php get_template_part('template-parts/content', 'header'); ?>
    
    <!-- .entry-content -->
    <div class="entry-content">
        <?php if (!is_single()) : // Only display Excerpts for Search ?>
            <?php the_excerpt(); ?>
        <?php else: ?>
            <?php
            the_content(
                sprintf(
                    wp_kses(
                        /* translators: %s: Name of current post. Only visible to screen readers */
                        __('Continue reading<span class="screen-reader-text"> "%s"</span>', 'neovantage'),
                        array(
                            'span' => array(
                                'class' => array(),
                            ),
                        )
                    ),
                    get_the_title()
                )
            );
            wp_link_pages(
                array(
                    'before' => '<div class="page-links">' . esc_html__('Pages:', 'neovantage'),
                    'after' => '</div>',
                )
            );
            ?>
        <?php endif; ?>
        <?php neovantage_entry_footer(); ?>
    </div><!-- .entry-content -->
    <?php if ('post' === get_post_type()) : ?>
        <div class="entry-meta clearfix">
            <?php neovantage_entry_meta(); ?>
            <?php if (!is_single()) : // Only display Excerpts for Search ?>
            <a href="<?php the_permalink(); ?>" rel="bookmark">
                <?php
                printf(
                    /* translators: %s: Name of current post. */
                    wp_kses( __('Continue reading %s <span class="meta-nav">&rarr;</span>', 'neovantage'),
                    array('span' => array('class' => array())) ), the_title('<span class="screen-reader-text">"', '"</span>', false)
                )
                ?>
            </a>
            <?php endif; ?>
        </div><!-- .entry-meta -->
    <?php endif; ?>
</article><!-- #post-## -->