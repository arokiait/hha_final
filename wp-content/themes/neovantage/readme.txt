=== NEOVANTAGE ===
Contributors: pixelspress, mohsinrafique
Author URI: https://pixelspress.com
Theme URI: https://pixelspress.com/free-wordpress-theme/
Tags: translation-ready, custom-background, theme-options, custom-menu, post-formats, threaded-comments

Requires at least: 4.4.2
Tested up to: 4.9.6
Stable tag: 1.2.5.2
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

NEOVANTAGE is a versatile sophisticated WordPress theme ideal for bloggers.

== Description ==

NEOVANTAGE is a versatile sophisticated WordPress theme ideal for bloggers. It's totally responsive so it adapts to your style as well as the device it's viewed on.

== License ==

NEOVANTAGE WordPress Theme, Copyright (C) 2016-2018, by PixelsPress.
NEOVANTAGE is distributed under the terms of the GNU GPL.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

NEOVANTAGE is based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc.
Underscores is distributed under the terms of the GNU GPL v2 or later.

NEOVANTAGE WordPress Theme bundles the following third-party resources:

== Credits ==

1. Underscores - http://underscores.me/
License: Distributed under the terms of the GPLv2 (https://www.gnu.org/licenses/gpl-2.0.html)
Copyright: 2012-2016 Automattic, Inc., https://automattic.com/

2. normalize.css - http://necolas.github.io/normalize.css/
License: Distributed under the terms of the MIT License (http://opensource.org/licenses/MIT)
Copyright: 2012-2016 Nicolas Gallagher and Jonathan Neal

3. Bootstrap - http://getbootstrap.com
License: Distributed under the terms of the MIT License (https://github.com/twbs/bootstrap/blob/master/LICENSE)
Copyright: 2011-2016 Twitter, Inc.

4. Lato Webfont - https://fonts.google.com/specimen/Lato
License: Distributed under the terms of the SIL Open Font License (http://scripts.sil.org/OFL)
Copyright: Łukasz Dziedzic

5. Montserrat Webfont - https://fonts.google.com/specimen/Montserrat
License: Distributed under the terms of the SIL Open Font License (http://scripts.sil.org/OFL)
Copyright: Julieta Ulanovsky

6. Open Sans Webfont - https://fonts.google.com/specimen/Open+Sans
License: Distributed under the terms of the SIL Open Font License (http://scripts.sil.org/OFL)
Copyright: Steve Matteson

7. Noto Serif Webfont - https://fonts.google.com/specimen/Noto+Serif
License: Distributed under the terms of the SIL Open Font License (http://scripts.sil.org/OFL)
Copyright: Google

8. Font Awesome Free 5.0.12 - https://fortawesome.github.io/Font-Awesome/
Copyright: 2012 Dave Gandy
Font License: SIL OFL 1.1
Code License: MIT License
http://fontawesome.io/license/

9. NEO Bootstrap Carousel - https://wordpress.org/plugins/neo-bootstrap-carousel/
License: Distributed under the terms of the GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
Copyright: 2017 PixelsPress, https://pixelspress.com

10. Contact Form 7 - https://wordpress.org/plugins/contact-form-7/
License: Distributed under the terms of the GPLv2 or later (http://www.gnu.org/licenses/gpl-2.0.html)
Copyright: Takayuki Miyoshi, https://ideasilo.wordpress.com/

11. Instagram Feed - https://wordpress.org/plugins/instagram-feed/
License: Distributed under the terms of the GPLv2 or later (http://www.gnu.org/licenses/gpl-2.0.html)
Copyright: 2017 Smash Balloon, https://smashballoon.com/

12. Images used in demo version are all from Pixabay - http://pixabay.com

Image 1: https://pixabay.com/en/post-it-notes-sticky-notes-note-1284667/
License: Distributed under the terms of the CC0 License (https://creativecommons.org/publicdomain/zero/1.0/)
Copyright: 2015 Pexels (https://pixabay.com/en/users/Pexels-2286921/)

Image 2: https://pixabay.com/en/spider-web-tree-branches-pattern-617769/
License: Distributed under the terms of the CC0 License (https://creativecommons.org/publicdomain/zero/1.0/)
Copyright: 2010-2015 skeeze (https://pixabay.com/en/users/skeeze-272447/)

Image 3: https://pixabay.com/en/calgary-cowboys-country-style-51454/
License: Distributed under the terms of the CC0 License (https://creativecommons.org/publicdomain/zero/1.0/)
Copyright: 2012 werner22brigitte (https://pixabay.com/en/users/werner22brigitte-5337/)

Image 4: https://pixabay.com/en/retro-vintage-record-player-case-731836/
License: Distributed under the terms of the CC0 License (https://creativecommons.org/publicdomain/zero/1.0/)
Copyright: 2015 SnapwireSnaps (https://pixabay.com/en/users/SnapwireSnaps-692569/)

Image 5: https://pixabay.com/en/clock-time-watch-fashion-hours-407101/
License: Distributed under the terms of the CC0 License (https://creativecommons.org/publicdomain/zero/1.0/)
Copyright: 2014 SplitShire (https://pixabay.com/en/users/SplitShire-364019/)

Image 6: https://pixabay.com/en/little-girl-pokemon-pikachu-lonely-1611389/
License: Distributed under the terms of the CC0 License (https://creativecommons.org/publicdomain/zero/1.0/)
Copyright: 2012 DigiPD (https://pixabay.com/en/users/DigiPD-57147/)

Image 7: https://pixabay.com/en/shelf-books-library-reading-159852/
License: Distributed under the terms of the CC0 License (https://creativecommons.org/publicdomain/zero/1.0/)
Copyright: 2013 OpenClipart-Vectors (https://pixabay.com/en/users/OpenClipart-Vectors-30363/)

Image 8: https://pixabay.com/en/yes-message-design-note-text-1510054/
License: Distributed under the terms of the CC0 License (https://creativecommons.org/publicdomain/zero/1.0/)
Copyright: 2016 Aktim (https://pixabay.com/en/users/Aktim-1683993/)

Image 9: https://pixabay.com/en/chairs-metal-event-rows-of-seats-1738739/
License: Distributed under the terms of the CC0 License (https://creativecommons.org/publicdomain/zero/1.0/)
Copyright: 2016 MonikaP (https://pixabay.com/en/users/MonikaP-2515080/)

Image 10: https://pixabay.com/en/code-coding-website-html-language-1557582/
License: Distributed under the terms of the CC0 License (https://creativecommons.org/publicdomain/zero/1.0/)
Copyright: 2016 TheDigitalWay (https://pixabay.com/en/users/TheDigitalWay-3008341/)

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click on the 'Activate' button to use your new theme right away.
4. Go to https://pixelspress.com/documentation for a guide on how to customize this theme.
5. Navigate to Appearance > Customize in your admin panel and customize to taste.


== Frequently Asked Questions ==

= Does this theme recommended any plugins? =

NEOVANTAGE recommended following list of plugins which are NEOVANTAGE Core, NEO Bootstrap Carousel, Instagram Feed, Contact Form 7

== Changelog ==

= 1.2.5.2 - 23 May, 2018 =
* Fix: Mobile menu close icon fixed

= 1.2.5.1 - 15 May, 2018 =
* Note: Updated documentation link

= 1.2.5 - 14 May, 2018 =
* Update: Author page is revised with author box

= 1.2.4.3 - 07 May, 2018 =
* Fix: Resolved post format icons placement on post listing page.

= 1.2.4.2 - 07 May, 2018 =
* Fix: Resolved is_plugin_active() fatal error on front end

= 1.2.4.1 - 07 May, 2018 =
* Note: Minor Tweaks in Structure

= 1.2.4 - 07 May, 2018 =
* Feature: Added Post View Count
* Fix: Resolved Fontawesome missing icons

= 1.2.3 - 06 May, 2018 =
* Feature: Social Media Follow Links added to Theme Options
* Update: Fontawesome updated to latest version – 5.0.12

= 1.2.2 - 06 May, 2018 =
* Fix: Resolved empty string check for colour variable from customizer

= 1.2.1 - 05 May, 2018 =
* Update: Add custom HTML code in <head></head>, just after <body>  and before closing </body> tag from Theme Options Panel

= 1.2 - 05 May, 2018 =
* Update: Improved Theme Options Panel
* Update: Instagram Feed updated to latest version – 1.8.3
* Update: Contact Form 7 updated to latest version – 5.0.1
* Update: NEOVANTAGE Core updated to latest version – 1.0.5

= 1.1.4 - 15 November, 2017 =
* Note: Updated TGM class

= 1.1.3 - 11 November, 2017 =
* Note: Clean up CSS
* Note: Updated Core Plugin
* Fix: Fixed responsive video size issue.

= 1.1.2 - 17 October, 2017 =
* Fix: Revised Post Formats Structure
* Fix: Resolved opengraph URL permalink link

= 1.1.1 - 09 August, 2017 =
* Feature: Create and manage knowledge base for your product
* Feature: Added Open Graph Meta Tags

= 1.1.0 - 17 April, 2017 =
* Feature: Added preloader to the theme
* Note: Added theme options from WP Customizer & removed the dependability of Redux Framework
* Note: Sidebar starting tag changed from "div" to "section"
* Note: Removed custom posts and page pagination functions & added default function the_posts_pagination and the_post_navigation.
* Note: Removed word limit custom excerpt function and added default filter to control the length of the words.
* Note: Revised overall HTML structure of content section
* Note: Post Formats Icons are updated.
* Update: Contact Form 7 updated to latest version – 4.7
* Fix: Resolved submenu typography
* Fix: Resolved post meta floating issues.
* Fix: Resolved sidebar layout issue in template-slideshow.php file

= 1.0.10 - 22 February, 2017 =
* Note: Hide page header in full width template

= 1.0.9 - 21 February, 2017 =
* Note: Updated Theme URI

= 1.0.8 - 21 February, 2017 =
* Note: Updated License for Font Awesome

= 1.0.7 - 20 February, 2017 =
* Fix: Resolved undefined variable: layout_class issue
* Fix: Resolved Color Option in Customizer
* Note: Added theme prefix(neovantage) handler in "add_image_size".
* Note: Removed unused rtl.css from theme directory.
* Note: Used antispambot function to escape email.

= 1.0.6 - 23 January, 2017 =
* Fix: Resolved Post Format Icon bug
* Note: Added WordPress Text Editor Stylesheet

= 1.0.5 - 09 January, 2017 =
* Fix: Resolved Post Thumbnail Icon layout bug
* Fix: Resolved Ping back URL bug
* Note: Updated readme.txt file

= 1.0.4 - 04 January, 2017 =
* Feature: Add custom logos.
* Fix: Bug resolved

= 1.0.3 - 23 November, 2016 =
* Note: Plugins removed from required list to recommended list.
* Note: Provided unique prefix for everything within the theme for public namespaces, functions, including options, global variables, post meta etc.
* Note: Included all the resources within theme.
* Note: Clean up code 

= 1.0.2 - 12 October, 2016 =
* Fix: "Trying to get property of non-object" with Custom Walker for wp_nav_menu
* Note: Removed Flex Slider
* Note: Clean up code

= 1.0.1 - 08 October, 2016 =
* Fix: Resolved wrong title on the Blog Page
* Note: Optimized theme CSS

= 1.0.0 - 28 August, 2016 =
* Initial release