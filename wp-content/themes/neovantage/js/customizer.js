/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */
;(function($) {
    
    var c = wp.customize;
    
    // Site title and description.
    c('blogname', function(value) {
        value.bind(function(to) {
            $('.site-title a').text(to);
        });
    });
    c('blogdescription', function(value) {
        value.bind(function(to) {
            $('.site-description').text(to);
        });
    });

    // Add custom-background-image body class when background image is added.
    c('background_image', function(value) {
        value.bind(function(to) {
            $('body').toggleClass('custom-background-image', '' !== to);
        });
    });
    
    c('_neovantage_theme_color', function (value) {
        value.bind(function (to) {
            $('.btn-primary,.btn-primary.disabled:hover,.btn-primary[disabled]:hover,fieldset[disabled] .btn-primary:hover,.btn-primary.disabled:focus,.btn-primary[disabled]:focus,fieldset[disabled] .btn-primary:focus,.btn-primary.disabled.focus,.btn-primary[disabled].focus,fieldset[disabled] .btn-primary.focus,.pagination > .active > a,.pagination > .active > span,.pagination > .active > a:hover,.pagination > .active > span:hover,.pagination > .active > a:focus,.pagination > .active > span:focus,.label-primary,.list-group-item.active,.list-group-item.active:hover,.list-group-item.active:focus,#featured,.input-group-addon .btn-default,.input-group-addon .btn-default:hover,.input-group-addon .btn-default:focus,.page-header,.navigation.post-navigation .nav-links a:hover,.navigation.post-navigation .nav-links a:focus,#pagination .neovantage-empty:hover')
                .css('background', to);
$('a:hover,a:focus,h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6,.text-primary,.bg-primary,.btn-primary .badge,.btn-link:hover,.btn-link:focus,.pagination > li > a:hover,.pagination > li > span:hover,.pagination > li > a:focus,.pagination > li > span:focus,article.entry-header h2 a,.btn-primary .badge,.btn-link:hover,.btn-link:focus,#secondary .widget .widget-title a,article .entry-header h2 a,.neovantage-main-menu .main-nav li > a:hover,.neovantage-main-menu .main-nav li .links-menu .sub-menu li:hover > a,.neovantage-main-menu .main-nav .current-menu-item a,.neovantage-main-menu .no-nav,.neovantage-mobile-menu .mobile-nav i:hover,.neovantage-mobile-menu .mobile-nav li a:hover,.navigation.pagination .nav-links .current,article .entry-header h2 a')
                .css('color', to);
            $('blockquote')
                .css('border-left-color', to);
            $('.blockquote-reverse,blockquote.pull-right')
                .css('border-right-color', to);
            $('.pagination > .active > a,.pagination > .active > span,.pagination > .active > a:hover,.pagination > .active > span:hover,.pagination > .active > a:focus,.pagination > .active > span:focus,.list-group-item.active,.list-group-item.active:hover,.list-group-item.active:focus,.input-group-addon .btn-default,.input-group-addon .btn-default:hover,.input-group-addon .btn-default:focus')
                .css('border-color', to);
        });
    });
})(jQuery);