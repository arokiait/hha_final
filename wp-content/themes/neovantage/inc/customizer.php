<?php
/**
 * NEOVANTAGE Theme Customizer.
 *
 * @package NEOVANTAGE
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function neovantage_customize_register( $wp_customize )
{
    /**
     * Failsafe is safe
     */
    if ( ! isset( $wp_customize ) ) {
        return;
    }
        
    $wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
    
    /**
     * NEOVANTAGE Theme Options Panel.
     * 
     * @since 1.2
     * 
     * @uses $wp_customize->add_panel() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_panel/
     * @link $wp_customize->add_panel() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_panel
     */
    $wp_customize->add_panel( 'neovantage_panel', array(
        'priority' => 30,
        'title' => __( 'NEOVANTAGE Theme Options', 'neovantage' ),
        'description' => __( 'Configure basic options for the NEOVANTAGE Theme', 'neovantage' ),
    ));
    
    $wp_customize->get_section( 'title_tagline' )->panel = "neovantage_panel";
    
    $wp_customize->get_section( 'colors' )->priority = "250";
    $wp_customize->get_section( 'colors' )->panel = "neovantage_panel";
    
    $wp_customize->add_setting( '_neovantage_theme_color', array(
        'default' => '#00897b',
        'sanitize_callback' => 'neovantage_sanitize_hex_color',
        'sanitize_js_callback' => 'maybe_hash_hex_color',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, '_neovantage_theme_color', array( // $args
        'priority' => 10,
        'section' => 'colors',
        'label' => __('Theme Color', 'neovantage'),
        'description' => __('Controls the main highlight color throughout the theme.', 'neovantage'),
    )));
    
    /**
     * Add Section for Basic Settings.
     * 
     * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
     * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
     */
    $wp_customize->add_section ( 'neovantage_section_basic', array( // $args
        'priority' => 160,
        'panel' => 'neovantage_panel',
        'title' => __( 'Basic Settings', 'neovantage' ),
        'description' => __( 'Here you will set your site-wide preferences.', 'neovantage' ),
    ));
    
    /**
     * Maintenance Mode setting.
     *
     * - Setting: Maintenance Mode
     * - Control: checkbox
     * - Sanitization: checkbox
     * 
     * Uses a checkbox to configure the maintenance mode.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting( 'maintenance_mode_setting', array( // $args
        'default' => '',
        'sanitize_callback' => 'neovantage_sanitize_checkbox'
    ));
    
    /**
     * Basic Checkbox control.
     *
     * - Control: Checkbox
     * - Setting: Enable/Disable Maintenance Mode
     * - Sanitization: checkbox
     * 
     * Register the core "checkbox" control to be used to enable/disbale Maintenance Mode setting.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control( 'maintenance_mode_control', array( // $args
        'settings' => 'maintenance_mode_setting',
        'section' => 'neovantage_section_basic',
        'label' => __( 'Enable Maintenance Mode', 'neovantage' ),
        'description' => __( 'Site visitors will see a site logo with the message you set below.', 'neovantage' ),
        'type' => 'checkbox',
    ));
    
    /**
     * Maintenance Message setting.
     *
     * - Setting: Maintenance Message
     * - Control: textarea
     * - Sanitization: html
     * 
     * Uses a textarea to configure the Maintenance Message for the Theme.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting( 'maintenance_msg_setting', array( // $args
        'default' => 'We are currently in maintenance mode. Please check back later.',
        'sanitize_callback' => 'neovantage_sanitize_html',
        'transport' => 'postMessage'
    ));
    
    /**
     * Basic Textarea control.
     *
     * - Control: Textarea
     * - Setting: Maintenance Message
     * - Sanitization: html
     * 
     * Register the core "textarea" control to be used to configure the Maintenance Message for the Theme.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control( 'maintenance_msg_setting', array( // $args
        'settings' => 'maintenance_msg_setting',
        'section' => 'neovantage_section_basic',
        'type' => 'textarea',
        'label' => __( 'Maintenance Message', 'neovantage' ),
        'description' => __( 'Message to show in maintenance mode', 'neovantage' ),
    ));
    
    /**
     * Preloader setting.
     *
     * - Setting: Preloader
     * - Control: select
     * - Sanitization: select
     * 
     * Uses a dropdown select to enable/disable the preloader.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting( 'preloader', array( // $args
        'default' => 'enable',
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'neovantage_sanitize_select'
    ));
    
    /**
     * Basic Select control.
     *
     * - Control: Select
     * - Setting: Preloader
     * - Sanitization: select
     * 
     * Register the core "select" control to be used to enable/disable the Preloader setting.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control( 'preloader', array( // $args
        'settings' => 'preloader',
        'section' => 'neovantage_section_basic',
        'type' => 'select',
        'label' => __( 'Preloader', 'neovantage' ),
        'description' => __( 'Select enable to be used the preloader feature for your site.', 'neovantage' ),
        'choices' => array(
            'enable' => __( 'Enable', 'neovantage' ),
            'disable' => __( 'Disable', 'neovantage' )
        )
    ));
    
    /**
     * Page Layout setting.
     *
     * - Setting: Page Layout
     * - Control: select
     * - Sanitization: select
     * 
     * Uses a dropdown select to configure the Page Layout setting.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting( 'page_layout', array( // $args
        'default' => 'sidebar-right',
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'neovantage_sanitize_select'
    ));
    
    /**
     * Basic Select control.
     *
     * - Control: Select
     * - Setting: Page Layout
     * - Sanitization: select
     * 
     * Register the core "select" control to be used to configure the Page Layout setting.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control( 'page_layout', array( // $args
        'settings' => 'page_layout',
        'section' => 'neovantage_section_basic',
        'type' => 'select',
        'label' => __( 'Page Layout', 'neovantage' ),
        'description' => __( 'Choose a default page layout for your pages: Sidebar Right, Sidebar Left or Fullwidth.', 'neovantage' ),
        'choices' => array(
            'sidebar-right' => __( 'Sidebar Right (default)', 'neovantage' ),
            'sidebar-left' => __( 'Sidebar Left', 'neovantage' ),
            'no-sidebar' => __( 'No Sidebar', 'neovantage' ),
        )
    ));
    
    /**
     * Add Section for Blog.
     * 
     * @since 1.2
     * 
     * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
     * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
     */
    $wp_customize->add_section ( 'neovantage_section_blog',  array( // $args
            'priority' => 300,
            'panel' => 'neovantage_panel',
            'title' => __( 'Blog', 'neovantage' ),
            'description' => __( 'Settings related to blog.', 'neovantage' )
        )
    );
    
    /**
     * Get Image from Content setting.
     *
     * - Setting: Get Image from Content
     * - Control: checkbox
     * - Sanitization: checkbox
     * 
     * Uses a checkbox to get the image from content.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting ( 'get_image_from_content_setting', array( // $args
        'default' => TRUE,
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'neovantage_sanitize_checkbox'
    ));
    
    /**
     * Basic Checkbox control.
     *
     * - Control: Checkbox
     * - Setting: Get Image from Content
     * - Sanitization: checkbox
     * 
     * Register the core "checkbox" control to be used to get image from content setting.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control ( 'get_image_from_content_control', array( // $args
        'settings' => 'get_image_from_content_setting',
        'section' => 'neovantage_section_blog',
        'label' => __( 'Get Featured Image from Content', 'neovantage' ),
        'description' => __( 'If you have not set a Featured image allow the system to show the first image from post content on archive pages.', 'neovantage' ),
        'type' => 'checkbox',
    ));
    
    /**
     * Add Section for Footer.
     * 
     * @since 1.2
     * 
     * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
     * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
     */
    $wp_customize->add_section( 'neovantage_section_footer', array( // $args
        'priority' => 400,
        'panel' => 'neovantage_panel',
        'title' => __( 'Footer', 'neovantage' ),
        'description' => __( 'Settings related to footer.', 'neovantage' )
    ));
    
    /**
     * Footer Widgets Area setting.
     *
     * - Setting: Footer Widgets Area
     * - Control: checkbox
     * - Sanitization: checkbox
     * 
     * Uses a checkbox to enable/disable the Footer Widgets Area.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting( 'footer_widget_area_setting', array( // $args
        'default' => FALSE,
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'neovantage_sanitize_checkbox',
        'type' => 'theme_mod',
    ));
    
    /**
     * Basic Checkbox control.
     *
     * - Control: Checkbox
     * - Setting: Footer Widgets Area
     * - Sanitization: checkbox
     * 
     * Register the core "checkbox" control to be used to enable/disable Footer Widgets Area setting.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control( 'footer_widget_area_control', array( // $args
        'settings' => 'footer_widget_area_setting',
        'section' => 'neovantage_section_footer',
        'label' => __( 'Footer Widgets Area', 'neovantage' ),
        'description' => __( 'Enable/Disable the Footer Widgets area globally. Please visit Appearance/Widgets menu to add new widgets!', 'neovantage' ),
        'type' => 'checkbox',
    ));
    
    /**
     * Footer Copyright Text setting example.
     *
     * - Setting: Footer Copyright Text
     * - Control: textarea
     * - Sanitization: html
     * 
     * Uses a text field to configure the user's copyright text displayed in the site footer.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting( 'footer_copyright_text', array( // $args
        'default' => sprintf(
                __( 'NEOVANTAGE Theme by <a rel="designer" href="%s">%s</a> <span class="sep"> | </span> Powered by <a rel="designer" href="%s">%s</a>', 'neovantage' ),
                esc_url( 'pixelspress.com' ),
                esc_html( 'PixelsPress' ),
                esc_url( 'wordpress.org' ),
                esc_html( 'WordPress' )
        ),
        'type' => 'theme_mod',
        'capability' => 'edit_theme_options',
        'sanitize_callback'	=> 'neovantage_sanitize_html'
    ));
    
    /**
     * Basic Text control.
     *
     * - Control: Basic: textarea
     * - Setting: Footer Copyright Text
     * - Sanitization: html
     * 
     * Register the core "text" control to be used to configure the Footer Copyright Text setting.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control( 'footer_copyright_text', array( // $args
        'settings' => 'footer_copyright_text',
        'section' => 'neovantage_section_footer',
        'label' => __( 'Footer Copyright Text', 'neovantage' ),
        'description' => __( 'Copyright or other text to be displayed in the site footer. HTML allowed.', 'neovantage' ),
        'type' => 'textarea',
    ));
    
    /**
     * Add Social Media Section
     * 
     * @since 1.2.2
     */
    $wp_customize->add_section('neovantage_social_media', array(
        'priority' => 450,
        'panel' => 'neovantage_panel',
        'title' => __( 'Social Media', 'neovantage' ),
        'description' => __( 'This tab controls the social networks that display in the footer. Add the network of your choice along with your unique URL. Each network you wish to display must be added here to show up in the footer.', 'neovantage' ),
    ));
    
    /**
     * Facebook Follow Link Setting
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Social Media Section
     * - Setting: Facebook Follow Link
     * - Sanitization: URL
     * 
     * Uses a URL text field to configure the company social link URL displayed in the site footer.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting ( 'facebook_link', array( // $args
        'default' => '',
        'sanitize_callback' => 'neovantage_sanitize_url'
    ));
    
    /**
     * Basic URL control.
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Social Media Section
     * - Setting: Facebook Follow Link
     * - Control: URL
     * 
     * Register the core "URL" text control to be used to configure the Facebook Link setting.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control ( 'facebook_link', array( // $args
        'section' => 'neovantage_social_media',
        'label' => __( 'Facebook', 'neovantage' ),
        'description' => __( 'Follow link URL to be displayed in the site footer.', 'neovantage' ),
        'type' => 'url',
    ));
    
    /**
     * Twitter Follow Link Setting
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Social Media Section
     * - Setting: Twitter Follow Link
     * - Sanitization: URL
     * 
     * Uses a URL text field to configure the company social link URL displayed in the site footer.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting( 'twitter_link', array( // $args
        'sanitize_callback' => 'neovantage_sanitize_url'
    ));
    
    /**
     * Basic URL control.
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Social Media Section
     * - Setting: Twitter Follow Link
     * - Control: URL
     * 
     * Register the core "URL" text control to be used to configure the Twitter Link setting.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control( 'twitter_link', array( // $args
        'section' => 'neovantage_social_media',
        'label' => __( 'Twitter', 'neovantage' ),
        'description' => __( 'Follow link URL to be displayed in the site footer.', 'neovantage' ),
        'type' => 'url',
    ));
    
    /**
     * Google+ Follow Link Setting
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Social Media Section
     * - Setting: Google+ Follow Link
     * - Sanitization: URL
     * 
     * Uses a URL text field to configure the company social link URL displayed in the site footer.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting ( 'google_plus_link',  array( // $args
        'sanitize_callback' => 'neovantage_sanitize_url'
    ));
    
    /**
     * Basic URL control.
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Social Media Section
     * - Setting: Google+ Follow Link
     * - Control: URL
     * 
     * Register the core "URL" text control to be used to configure the Google+ Link setting.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control ( 'google_plus_link', array( // $args
        'section' => 'neovantage_social_media',
        'label' => __( 'Google+', 'neovantage' ),
        'description' => __( 'Follow link URL to be displayed in the site footer.', 'neovantage' ),
        'type' => 'url'
    ));
    
    /**
     * Instagram Follow Link Setting
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Social Media Section
     * - Setting: Instagram Follow Link
     * - Sanitization: URL
     * 
     * Uses a URL text field to configure the company social link URL displayed in the site footer.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting ( 'instagram_link',  array( // $args
        'sanitize_callback' => 'neovantage_sanitize_url'
    ));
    
    /**
     * Basic URL control.
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Social Media Section
     * - Setting: Instagram Follow Link
     * - Control: URL
     * 
     * Register the core "URL" text control to be used to configure the Instagram Link setting.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control ( 'instagram_link', array( // $args
        'section' => 'neovantage_social_media',
        'label' => __( 'Instagram', 'neovantage' ),
        'description' => __( 'Follow link URL to be displayed in the site footer.', 'neovantage' ),
        'type' => 'url'
    ));
    
    /**
     * LinkedIn Follow Link Setting
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Social Media Section
     * - Setting: LinkedIn Follow Link
     * - Sanitization: URL
     * 
     * Uses a URL text field to configure the company social link URL displayed in the site footer.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting( 'linkedIn_link', array( // $args
        'sanitize_callback' => 'neovantage_sanitize_url'
    ));
    
    /**
     * Basic URL control.
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Social Media Section
     * - Setting: LinkedIn Follow Link
     * - Control: URL
     * 
     * Register the core "URL" text control to be used to configure the LinkedIn Link setting.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control( 'linkedIn_link', array( // $args
        'section' => 'neovantage_social_media',
        'label' => __( 'LinkedIn', 'neovantage' ),
        'description' => __( 'Follow link URL to be displayed in the site footer.', 'neovantage' ),
        'type' => 'url',
    ));
    
    /**
     * Pinterest Follow Link Setting
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Social Media Section
     * - Setting: Pinterest Follow Link
     * - Sanitization: URL
     * 
     * Uses a URL text field to configure the company social link URL displayed in the site footer.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting( 'pinterest_link', array( // $args
        'sanitize_callback' => 'neovantage_sanitize_url'
    ));
    
    /**
     * Basic URL control.
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Social Media Section
     * - Setting: Pinterest Follow Link
     * - Control: URL
     * 
     * Register the core "URL" text control to be used to configure the Pinterest Link setting.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control( 'pinterest_link', array( // $args
        'section' => 'neovantage_social_media',
        'label' => __( 'Pinterest', 'neovantage' ),
        'description' => __( 'Follow link URL to be displayed in the site footer.', 'neovantage' ),
        'type' => 'url',
    ));
    
    /**
     * StumbleUpon Follow Link Setting
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Social Media Section
     * - Setting: StumbleUpon Follow Link
     * - Sanitization: URL
     * 
     * Uses a URL text field to configure the company social link URL displayed in the site footer.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting( 'stumbleupon_link', array( // $args
        'sanitize_callback' => 'neovantage_sanitize_url'
    ));
    
    /**
     * Basic URL control.
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Social Media Section
     * - Setting: StumbleUpon Follow Link
     * - Control: URL
     * 
     * Register the core "URL" text control to be used to configure the StumbleUpon Link setting.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control( 'stumbleupon_link', array( // $args
        'section' => 'neovantage_social_media',
        'label' => __( 'StumbleUpon', 'neovantage' ),
        'description' => __( 'Follow link URL to be displayed in the site footer.', 'neovantage' ),
        'type' => 'url',
    ));
    
    /**
     * Add Custom HTML Code Section
     * 
     * @since 1.2
     */
    $wp_customize->add_section('neovantage_custom_html_code', array(
        'priority' => 500,
        'panel' => 'neovantage_panel',
        'title' => __( 'Custom HTML Code', 'neovantage' ),
    ));
    
    /**
     * Codes Between <head> and </head> Tags Settings
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Custom HTML Code Section
     * - Setting: Codes Between <head> and </head> Tags
     * - Sanitization: html
     * 
     * Uses a textarea to configure the Head Tag Code for the Theme.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting( '_neovantage_code_head', array(
        'transport' => 'postMessage',
        'sanitize_callback' => 'neovantage_sanitize_none',
    ));
    
    /**
     * Basic Textarea control.
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Custom HTML Code Section
     * - Setting: Codes Between <head> and </head> Tags
     * - Control: textarea
     * 
     * Register the core "textarea" control to be used to configure the Code Tag for the Theme.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control ( '_neovantage_code_head', array( // $args
        'section' => 'neovantage_custom_html_code',
        'label' => __( 'Codes between <head> and </head> tags', 'neovantage' ),
        'description' => __( 'Please be careful. Bad codes can make invalidation issue.', 'neovantage' ),
        'type' => 'textarea',
    ));
    
    /**
     * Codes right after <body> tag Settings
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Custom HTML Code Section
     * - Setting: Codes right after <body> tag
     * - Sanitization: html
     * 
     * Uses a textarea to configure the right after Body Tag Code for the Theme.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting( '_neovantage_code_body_start', array(
        'transport' => 'postMessage',
        'sanitize_callback' => 'neovantage_sanitize_none',
    ));
    
    /**
     * Basic Textarea control.
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Custom HTML Code Section
     * - Setting: Codes right after <body> tag
     * - Control: textarea
     * 
     * Register the core "textarea" control to be used to configure the Code Tag after body tag for the Theme.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control ( '_neovantage_code_body_start', array( // $args
        'section' => 'neovantage_custom_html_code',
        'label' => __( 'Codes right after <body> tag', 'neovantage' ),
        'description' => __( 'Please be careful. Bad codes can make invalidation issue.', 'neovantage' ),
        'type' => 'textarea',
    ));
    
    /**
     * Codes right before </body> tag Settings
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Custom HTML Code Section
     * - Setting: Codes right before </body> tag
     * - Sanitization: html
     * 
     * Uses a textarea to configure the right after Body Tag Code for the Theme.
     * 
     * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
     * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
     */
    $wp_customize->add_setting( '_neovantage_code_body_end', array(
        'transport' => 'postMessage',
        'sanitize_callback' => 'neovantage_sanitize_none',
    ));
    
    /**
     * Basic Textarea control.
     *
     * - Panel: NEOVANTAGE Theme Options Panel
     * - Section: Add Custom HTML Code Section
     * - Setting: Codes right before </body> tag
     * - Control: textarea
     * 
     * Register the core "textarea" control to be used to configure the Code Tag after body tag for the Theme.
     * 
     * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
     * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
     */
    $wp_customize->add_control ( '_neovantage_code_body_end', array( // $args
        'section' => 'neovantage_custom_html_code',
        'label' => __( 'Codes right before </body> tag', 'neovantage' ),
        'description' => __( 'Please be careful. Bad codes can make invalidation issue.', 'neovantage' ),
        'type' => 'textarea',
    ));
}
add_action( 'customize_register', 'neovantage_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function neovantage_customize_preview_js()
{
    wp_enqueue_script( 'neovantage_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'neovantage_customize_preview_js' );

/**
 * Select sanitization callback.
 *
 * - Sanitization: select
 * - Control: select, radio
 * 
 * Sanitization callback for 'select' and 'radio' type controls. This callback sanitizes `$input`
 * as a slug, and then validates `$input` against the choices defined for the control.
 * 
 * @see sanitize_key()               https://developer.wordpress.org/reference/functions/sanitize_key/
 * @see $wp_customize->get_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/get_control/
 *
 * @param string               $input   Slug to sanitize.
 * @param WP_Customize_Setting $setting Setting instance.
 * @return string Sanitized slug if it is a valid choice; otherwise, the setting default.
 */
function neovantage_sanitize_select( $input, $setting )
{	
    // Ensure input is a slug.
    $input = sanitize_key( $input );

    // Get list of choices from the control associated with the setting.
    $choices = $setting->manager->get_control( $setting->id )->choices;

    // If the input is a valid key, return it; otherwise, return the default.
    return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}

/**
 * Widget sanitization callback.
 * 
 * @param string $value
 * @return string
 */
function neovantage_sanitize_widget_area( $value )
{
    if ( !in_array( $value, array( 'enable', 'disable' ) ) ) {
        $value = 'disable';
    }
    return $value;
}

/**
 * Checkbox sanitization callback.
 * 
 * Sanitization callback for 'checkbox' type controls. This callback sanitizes `$checked`
 * as a boolean value, either TRUE or FALSE.
 *
 * @param bool $checked Whether the checkbox is checked.
 * @return bool Whether the checkbox is checked.
 */
function neovantage_sanitize_checkbox( $checked )
{
    // Boolean check.
    return ( ( isset( $checked ) && true == $checked ) ? true : false );
}

/**
 * HTML sanitization callback.
 *
 * - Sanitization: html
 * - Control: text, textarea
 * 
 * Sanitization callback for 'html' type text inputs. This callback sanitizes `$html`
 * for HTML allowable in posts.
 * 
 * 
 * @see wp_filter_post_kses() https://developer.wordpress.org/reference/functions/wp_filter_post_kses/
 *
 * @param string $html HTML to sanitize.
 * @return string Sanitized HTML.
 */
function neovantage_sanitize_html( $html )
{
    return wp_filter_post_kses( $html );
}

/**
 * URL sanitization callback.
 *
 * - Sanitization: url
 * - Control: text, url
 * 
 * Sanitization callback for 'url' type text inputs. This callback sanitizes `$url` as a valid URL.
 * 
 * NOTE: esc_url_raw() can be passed directly as `$wp_customize->add_setting()` 'sanitize_callback'.
 * It is wrapped in a callback here merely for example purposes.
 * 
 * @see esc_url_raw() https://developer.wordpress.org/reference/functions/esc_url_raw/
 *
 * @param string $url URL to sanitize.
 * @return string Sanitized URL.
 */
function neovantage_sanitize_url( $url ) {
    return esc_url_raw( $url );
}

/**
 * Sanitizes a hex color.
 * Returns either ”, a 3 or 6 digit hex color (with #), or nothing.
 * 
 * @since 1.2
 * 
 * @param string $color
 * @return string
 */
function neovantage_sanitize_hex_color( $color ) {
    if ( '' === $color ) {
        return '';
    }
 
    // 3 or 6 hex digits, or the empty string.
    if ( preg_match('|^#([A-Fa-f0-9]{3}){1,2}$|', $color ) ) {
        return $color;
    }
}
/**
 * HTML sanitization callback.
 *
 * - Sanitization: html
 * - Control: text, textarea
 * 
 * Sanitization callback for 'html' type text inputs. This callback sanitizes `$html`
 * for HTML allowable in posts.
 * 
 * 
 * @see wp_filter_post_kses() https://developer.wordpress.org/reference/functions/wp_filter_post_kses/
 *
 * @param string $html HTML to sanitize.
 * @return string Sanitized HTML.
 */
function neovantage_sanitize_none( $html )
{
    return $html;
}