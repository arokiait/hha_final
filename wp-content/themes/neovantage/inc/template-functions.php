<?php

/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package NEOVANTAGE
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function neovantage_body_classes($classes)
{
    // Adds a class of group-blog to blogs with more than 1 published author.
    if (is_multi_author()) {
        $classes[] = 'group-blog';
    }
    
    // Adds a class of no-sidebar to sites without active sidebar.
    if ( ! is_active_sidebar( 'sidebar-1' ) ) {
        $classes[] = 'no-sidebar';
    }

    // Adds a class of hfeed to non-singular pages.
    if (!is_singular()) {
        $classes[] = 'hfeed';
    }

    return $classes;
}
add_filter('body_class', 'neovantage_body_classes');

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function neovantage_pingback_header()
{
    if (is_singular() && pings_open()) {
        echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
    }
}
add_action('wp_head', 'neovantage_pingback_header');

if (!function_exists('neovantage_maintenance_mode')) :
    /**
     * Maintenance Mode
     */
    function neovantage_maintenance_mode()
    {
        $logo_img = '';
        $custom_logo_id = get_theme_mod( 'custom_logo' );
        if( isset( $custom_logo_id ) && !empty( $custom_logo_id ) ) {
            $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
            $logo_path = $image[0];
            $logo_img = '<img src="'. esc_url( $logo_path ) .'" alt="'. esc_attr__('Maintenance', 'neovantage').'" style="margin: 0 auto; display: block;" />';
        }
        $maintenance_mode_setting = get_theme_mod( 'maintenance_mode_setting', '0' );
        
        if ( !empty( $maintenance_mode_setting ) && '1' == $maintenance_mode_setting )
        {
            if ( !current_user_can( 'edit_themes' ) || !is_user_logged_in() ) {
                wp_die(
                    $logo_img 
                        . '<div style="text-align:center">'
                        . esc_textarea( get_theme_mod( 'maintenance_msg_setting', 'We are currently in maintenance mode. Please check back later.' ) )
                        . '</div>',
                    get_bloginfo( 'name' )
                );
            }
        }
    }
endif;
add_action('get_header', 'neovantage_maintenance_mode');

if ( ! function_exists( 'neovantage_add_opengraph_doctype' ) ) :

    /**
     * Added Open Graph in the Language Attributes
     * 
     * @since 1.1.1
     */
    function neovantage_add_opengraph_doctype( $output )
    {
        return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
    }
endif;
add_filter('language_attributes', 'neovantage_add_opengraph_doctype');

if (!function_exists('neovantage_social_meta')) :
    
    /**
     * Added Social Meta
     * 
     * @since 1.1.1
     */
    function neovantage_social_meta()
    {
        global $post;
        
        $logo_img = '';
        $custom_logo_id = get_theme_mod( 'custom_logo' );
        if( isset( $custom_logo_id ) && !empty( $custom_logo_id ) ) {
            $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
            $logo_path = $image[0];
        }

        $themes = wp_get_themes();

        // If we are on a blog post/page
        if (is_singular()) {
?>
            <meta property="og:title" content="<?php echo the_title(); ?>">
            <meta property="og:type" content="article">
            <meta property="og:url" content="<?php echo the_permalink(); ?>">
            <meta property="og:site_name" content="<?php bloginfo('name'); ?>">
<?php
            if (has_excerpt($post->ID)) {
?>
            <meta property="og:description" content="<?php wp_strip_all_tags( get_the_excerpt() ); ?>">
 <?php
            } else {
?>
            <meta property="og:description" content="<?php bloginfo('description'); ?>">
<?php
            }

            if (has_post_thumbnail($post->ID)) {
                $thumb = get_post_thumbnail_id();
                $img_url = wp_get_attachment_image_src($thumb, 'full');
                $img_url = $img_url[0];
            } else {
                $img_url = $logo_path;
            }
?>
            <meta property="og:image" content="<?php echo esc_url( $img_url ); ?>">
<?php
        } elseif (is_front_page() or is_home()) {
?>
        <meta property="og:title" content="<?php bloginfo("name"); ?>">
        <meta property="og:type" content="website">
        <meta property="og:url" content="<?php the_permalink(); ?>">
        <meta property="og:site_name" content="<?php bloginfo('name'); ?>">

<?php
            if (has_excerpt($post->ID)) {
?>
                <meta property="og:description" content="<?php echo wp_strip_all_tags( get_the_excerpt() ); ?>">
<?php
            } else {              
?>
        <meta property="og:description" content="<?php bloginfo('description'); ?>">
<?php
            }

            if (has_post_thumbnail($post->ID)) {
                $thumb = get_post_thumbnail_id();
                $img_url = wp_get_attachment_image_src($thumb, 'full');
                $img_url = $img_url[0];
            } else {
                $img_url = $logo_path;
            }
?>
            <meta property="og:image" content="<?php echo esc_url( $img_url ); ?>">
<?php
        }
    }
endif;
add_filter('wp_head', 'neovantage_social_meta', 2);

if (!function_exists('neovantage_excerpt_length')) :
    /**
     * Filter the except length to 20 words.
     *
     * @since 1.1.0
     * 
     * @param int $length Excerpt length.
     * @return int (Maybe) modified excerpt length.
     */
    function neovantage_excerpt_length( $length )
    {
        return 35;
    }
endif;
add_filter( 'excerpt_length', 'neovantage_excerpt_length', 999 );


if (!function_exists('neovantage_excerpt_more')) :
    /**
     * Filter the excerpt "read more" string.
     * 
     * @since 1.1.0
     * 
     * @param string $more "Read more" excerpt string.
     * @return string (Maybe) modified "read more" excerpt string.
     */
    function neovantage_excerpt_more( $more )
    {
        return ' ...';
    }
endif;
add_filter( 'excerpt_more', 'neovantage_excerpt_more' );

if (!function_exists('neovantage_custom_html_code_head')) :
    /**
     * Filter wp_head to add custom HTML code in head
     * 
     * @since 1.2
     * 
     * @param string $more "Read more" excerpt string.
     * @return string (Maybe) modified "read more" excerpt string.
     */
    function neovantage_custom_html_code_head( $more )
    {
        $neovantage_code_head = get_theme_mod('_neovantage_code_head','');
        
        if( '' != $neovantage_code_head) :
            echo $neovantage_code_head;
        endif;
    }
endif;
add_filter( 'wp_head', 'neovantage_custom_html_code_head' );


/**
 * Removes legacy contact fields and adds support for Facebook,Twitter, LinkedIn.
 *
 * @version 1.0.0
 * @since 1.2.5
 * 
 * @param array $fields  Array of default contact fields.
 * @return array $fields Amended array of contact fields.
 */
function neovantage_custom_contact_info( $fields ) {
     
    // Remove the old, unused fields.
    unset( $fields['aim'] );
    unset( $fields['yim'] );
    unset( $fields['jabber'] );
     
    // Add LinkedIn.
    $fields['author_email'] = __( 'Email (Author Page)', 'neovantage' );
    $fields['author_facebook'] = __( 'Facebook (Author Page)', 'neovantage' );
    $fields['author_twitter'] = __( 'Twitter (Author Page)', 'neovantage' );
    $fields['author_linkedin'] = __( 'LinkedIn (Author Page)', 'neovantage' );
    $fields['author_dribbble'] = __( 'Dribbble (Author Page)', 'neovantage' );
    $fields['author_gplus'] = __( 'Google+ (Author Page)', 'neovantage' );
     
    // Return the amended contact fields.
    return $fields;
     
}
add_filter( 'user_contactmethods', 'neovantage_custom_contact_info' );

/**
 * Change Primary Colour
 */
function neovantage_custom_styles() {
    
    $theme_color = get_theme_mod( '_neovantage_theme_color', FALSE );
    
    if( $theme_color ) :
?>
    <style type="text/css">
        a:hover, a:focus { color: <?php echo esc_attr($theme_color); ?>; }
        h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6 { color: <?php echo esc_attr($theme_color); ?>; }
        .text-primary { color: <?php echo esc_attr($theme_color); ?>; }
        .bg-primary { color: <?php echo esc_attr($theme_color); ?>; }
        blockquote { border-left-color: <?php echo esc_attr($theme_color); ?>; }
        .blockquote-reverse,blockquote.pull-right { border-right-color: <?php echo esc_attr($theme_color); ?>; }
        .btn-primary {
            background-color: <?php echo esc_attr($theme_color); ?>;
            border-color: <?php echo esc_attr($theme_color); ?>;
        }
        .btn.btn-primary:hover,
        .btn.btn-primary:focus,
        .btn.btn-primary:active,
        .btn.btn-primary::after {
            background-color: <?php echo neovantage_adjust_brightness(esc_attr($theme_color), -20); ?>;
            border-color: <?php echo neovantage_adjust_brightness(esc_attr($theme_color), -20); ?>;
        }
        
        .btn-primary.disabled:hover,
        .btn-primary[disabled]:hover,
        fieldset[disabled] .btn-primary:hover,
        .btn-primary.disabled:focus,
        .btn-primary[disabled]:focus,
        fieldset[disabled] .btn-primary:focus,
        .btn-primary.disabled.focus,
        .btn-primary[disabled].focus,
        fieldset[disabled] .btn-primary.focus {
            background-color: <?php echo esc_attr($theme_color); ?>;
        }
        .btn-primary .badge { color: <?php echo esc_attr($theme_color); ?>; }
        .btn-link:hover,.btn-link:focus { color: <?php echo esc_attr($theme_color); ?>; }
        
        .pagination > li > a:hover,
        .pagination > li > span:hover,
        .pagination > li > a:focus,
        .pagination > li > span:focus {
            color: <?php echo esc_attr($theme_color); ?>;
        }
        .pagination > .active > a,
        .pagination > .active > span,
        .pagination > .active > a:hover,
        .pagination > .active > span:hover,
        .pagination > .active > a:focus,
        .pagination > .active > span:focus {
            background-color: <?php echo esc_attr($theme_color); ?>;
            border-color: <?php echo esc_attr($theme_color); ?>;
        }
        .label-primary {
            background-color: <?php echo esc_attr($theme_color); ?>;
        }
        .list-group-item.active,
        .list-group-item.active:hover,
        .list-group-item.active:focus {
            background-color: <?php echo esc_attr($theme_color); ?>;
            border-color: <?php echo esc_attr($theme_color); ?>;
        }
        #featured {
            background: <?php echo esc_attr($theme_color); ?>;
        }
        
        article.entry-header h2 a,.btn-primary .badge,.btn-link:hover,.btn-link:focus
        {
            color: <?php echo esc_attr($theme_color); ?>;
        }
        
        /* =============================================================================
         * Components > Addon Group
         * ===========================================================================*/
        .input-group-addon .btn-default {
            background-color: <?php echo esc_attr($theme_color); ?>;
            border-color: <?php echo esc_attr($theme_color); ?>;
        }
        .input-group-addon .btn-default:hover,
        .input-group-addon .btn-default:focus {
            background-color: <?php echo esc_attr($theme_color); ?>;
            border-color: <?php echo esc_attr($theme_color); ?>;
        }
        /*------------------------------------------------------------------------------
         * Components > Widgets
         * ===========================================================================*/
        #secondary .widget .widget-title a { color: <?php echo esc_attr($theme_color); ?>; }
        /* =============================================================================
         * Blog > Article
         * ===========================================================================*/
        article .entry-header h2 a {
            color: <?php echo esc_attr($theme_color); ?>;
        }
        /* =============================================================================
         * NAVIGATION STYLES
         * ===========================================================================*/
        .neovantage-main-menu .main-nav li > a:hover {
            color: <?php echo esc_attr($theme_color); ?>;
        }
        .neovantage-main-menu .main-nav li .links-menu .sub-menu li:hover > a {
            color: <?php echo esc_attr($theme_color); ?>;
        }
        .neovantage-main-menu .main-nav .current-menu-item a {
            color: <?php echo esc_attr($theme_color); ?>;
        }
        .neovantage-main-menu .no-nav {
            color: <?php echo esc_attr($theme_color); ?>;
        }
        .neovantage-mobile-menu .mobile-nav i:hover {
            color: <?php echo esc_attr($theme_color); ?>;
        }
        .neovantage-mobile-menu .mobile-nav li a:hover {
            color: <?php echo esc_attr($theme_color); ?>;
        }
        /* =============================================================================
         * Components > Page Header
         * ===========================================================================*/
        .page-header {
            background-color: <?php echo esc_attr($theme_color); ?>;
        }
        /* =============================================================================
         * Components > Pagination
         * ===========================================================================*/
        .navigation.post-navigation .nav-links a:hover,
        .navigation.post-navigation .nav-links a:focus {
            background-color: <?php echo esc_attr($theme_color); ?>;
        }
        .navigation.pagination .nav-links .current {
            color: <?php echo esc_attr($theme_color); ?>;
        }
        #pagination .neovantage-empty:hover {
            background-color: <?php echo esc_attr($theme_color); ?>;
        }
        /* =============================================================================
         * Blog > Article
         * ===========================================================================*/
        article .entry-header h2 a {
            color: <?php echo esc_attr($theme_color); ?>;
        }
    </style>
<?php
    endif;
}
add_action( 'wp_head', 'neovantage_custom_styles', 100 );