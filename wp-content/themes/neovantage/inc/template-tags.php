<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package NEOVANTAGE
 */

/**
 * Prints HTML with meta information for the current post-date/time, author, categories, tags and comments.
 */
if ( ! function_exists( 'neovantage_entry_meta' ) ) :

    function neovantage_entry_meta()
    {
        global $post;
        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
        if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s" style="display: none;">%4$s</time>';
        }

        $time_string = sprintf(
            $time_string,
            esc_attr( get_the_date( 'c' ) ),
            esc_html( get_the_date() ),
            esc_attr( get_the_modified_date( 'c' ) ),
            esc_html( get_the_modified_date() )
        );
        $posted_on = sprintf(
            esc_html_x( '%s', 'post date', 'neovantage' ),
            '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
        );
        $byline = sprintf(
            esc_html_x( '%s', 'post author', 'neovantage' ),
            '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
        );

        $meta_html = '<ul class="meta-post">';
            $meta_html .= '<li><i class="far fa-calendar"></i> '. $posted_on .'</li>';
            $meta_html .= '<li><i class="far fa-user"></i> '. $byline .'</li>';

            // Hide categories text for pages.
            if ( 'post' === get_post_type() ) {
                /* translators: used between list items, there is a space after the comma */
                $categories_list = get_the_category_list( esc_html__( ', ', 'neovantage' ) );
                if ( $categories_list && neovantage_categorized_blog() ) {
                    $meta_html .= sprintf( '<li><i class="far fa-folder-open"></i> ' . esc_html__( '%1$s', 'neovantage' ) . '</li>', $categories_list ); // WPCS: XSS OK.
                }
                
                if ( !function_exists( 'is_plugin_active' ) ) {
                    /**
                     * Detect plugin. For use on Front End only.
                     */
                    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
                }
                if ( is_plugin_active( 'neovantage-core/neovantage-core.php' ) ) {
                    if( is_single() && class_exists('Neovantage_Core_Post_View_Count') ) :
                        $post_view_count = new Neovantage_Core_Post_View_Count;
                        $post_views = $post_view_count->neovantage_get_post_views($post->ID);
                        $meta_html .= sprintf( '<li><span class="post-views"><i class="fab fa-gripfire"></i> ' . esc_html__( '%1$s', 'neovantage' ) . '</span></li>', $post_views ); // WPCS: XSS OK.
                    endif;
                }
            }

            if ( !is_single() && !post_password_required() && ( comments_open() || get_comments_number() ) ) {
                $meta_html .= '<li><i class="far fa-comments"></i>';
                    $meta_html .= '<a href="'. esc_url( get_comments_link( $post->ID ) ) .'">';
                        $meta_html .= number_format_i18n( get_comments_number() );
                        if( number_format_i18n( get_comments_number() ) > 1 ) {
                            $meta_html .= __( ' Comments', 'neovantage' );
                        } else {
                            $meta_html .= __( ' Comment', 'neovantage' );
                        }
                    $meta_html .= '</a>';
                $meta_html .= '</li>';
            }
        $meta_html .= '</ul>';
        echo $meta_html;
    }
endif;

if (!function_exists('neovantage_entry_footer')) :

    /**
     * Prints HTML with meta information for the categories, tags and comments.
     */
    function neovantage_entry_footer()
    {
        // Hide tag text for pages.
        if ( is_single() && 'post' === get_post_type() ) {
            /* translators: used between list items, there is a space after the comma */
            $tags_list = get_the_tag_list( '', esc_html__( ', ', 'neovantage' ) );
            if ( $tags_list ) {
                printf( '<div class="tags-links"><i class="fas fa-tags"></i> ' . esc_html__( '%1$s', 'neovantage' ) . '</div>', $tags_list ); // WPCS: XSS OK.
            }
        }
    }
endif;

if ( ! function_exists( 'neovantage_edit_post' ) ) :
    
/**
 * Show edit link.
 */
function neovantage_edit_post()
{
    edit_post_link(
        sprintf(
            wp_kses(
                /* translators: %s: Name of current post. Only visible to screen readers */
                __( 'Edit <span class="screen-reader-text">%s</span>', 'neovantage' ),
                array(
                    'span' => array(
                        'class' => array(),
                    ),
                )
            ),
            get_the_title()
        ),
        '<span class="edit-link">',
        '</span>'
    );
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function neovantage_categorized_blog()
{
    if (false === ( $all_the_cool_cats = get_transient('neovantage_categories') )) {
        // Create an array of all the categories that are attached to posts.
        $all_the_cool_cats = get_categories(
            array(
                'fields' => 'ids',
                'hide_empty' => 1,
                // We only need to know if there is more than one category.
                'number' => 2,
            )
        );

        // Count the number of categories that are attached to the posts.
        $all_the_cool_cats = count($all_the_cool_cats);

        set_transient('neovantage_categories', $all_the_cool_cats);
    }

    if ($all_the_cool_cats > 1) {
        // This blog has more than 1 category so neovantage_categorized_blog should return true.
        return true;
    } else {
        // This blog has only 1 category so neovantage_categorized_blog should return false.
        return false;
    }
}

/**
 * Flush out the transients used in neovantage_categorized_blog.
 */
function neovantage_category_transient_flusher()
{
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    // Like, beat it. Dig?
    delete_transient('neovantage_categories');
}

add_action('edit_category', 'neovantage_category_transient_flusher');
add_action('save_post', 'neovantage_category_transient_flusher');

if (!function_exists('neovantage_breadcrumbs')) :
    /**
     * Breadcrumbs
     * 
     * @global object $wp_query
     * @global object $post
     * @global object $author
     */
    function neovantage_breadcrumbs()
    {
        global $wp_query, $post;
        
        /* === OPTIONS === */
        $text['home'] = '<i class="fas fa-home"></i>'; // text for the 'Home' link
        $text['category'] = '%s'; // text for a category page
        $text['search'] = '%s'; // text for a search results page
        $text['tag'] = '%s'; // text for a tag page
        $text['author'] = '%s'; // text for an author page
        $text['404'] = __('Error 404', 'neovantage'); // text for the 404 page

        $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
        $showOnHome = 1; // 1 - show breadcrumbs on the homepage, 0 - don't show
        $delimiter = ''; // delimiter between crumbs
        $before = '<li class="active">'; // tag before the current crumb
        $after = '</li>'; // tag after the current crumb
        /* === END OF OPTIONS === */
        $current_page = '<a title="' . esc_attr( get_the_title() ) . '" href="' . esc_url( get_permalink() ) . '">' . esc_attr( get_the_title() ) . '</a>';
        $homeLink = esc_attr( home_url() ) . '/';
        $linkBefore = '<li>';
        $linkAfter = '</li>';
        $linkAttr = '';
        $link = $linkBefore . '<a ' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;
        $linkhome = $linkBefore . '<a ' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;

        if (is_front_page()) {
            if ($showOnHome == "1")
                echo '<ul class="breadcrumb pull-right">' . $before . '<a href="' . $homeLink . '">' . $text['home'] . '</a>' . $after . '</ul>';
        } else {
            echo '<ul class="breadcrumb">' . sprintf($linkhome, $homeLink, $text['home']) . $delimiter;
            if (is_home()) {
                echo $before . __('Blog', 'neovantage') . $after;
            } elseif (is_category()) {
                $thisCat = get_category(get_query_var('cat'), false);
                if ($thisCat->parent != 0) {
                    $cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
                    $cats = str_replace('">', '">', $cats);
                    $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
                    $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
                    echo esc_attr($cats);
                }
                echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;
            } elseif (is_search()) {
                echo $before . sprintf($text['search'], get_search_query()) . $after;
            } elseif (is_day()) {
                echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
                echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F')) . $delimiter;
                echo $before . get_the_time('d') . $after;
            } elseif (is_month()) {
                echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
                echo $before . get_the_time('F') . $after;
            } elseif (is_year()) {
                echo $before . get_the_time('Y') . $after;
            } elseif (is_single() && !is_attachment()) {
                if (get_post_type() != 'post') {
                    $post_type = get_post_type_object(get_post_type());
                    $slug = $post_type->rewrite;
                    printf($link, $homeLink . $slug['slug'] . '/', $post_type->labels->name);
                    if ($showCurrent == 1)
                        echo $delimiter . $before . $current_page . $after;
                } else {
                    $cat = get_the_category();
                    $cat = $cat[0];

                    $cats = get_category_parents($cat, TRUE, $delimiter);

                    if ($showCurrent == 0)
                        $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);

                    $cats = str_replace('">', '">', $cats);
                    $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
                    $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
                    echo $cats;
                    if ($showCurrent == 1)
                        echo $before . $current_page . $after;
                }
            } elseif (!is_single() && !is_page() && get_post_type() <> '' && get_post_type() != 'post' && get_post_type() <> 'events' && !is_404()) {
                $post_type = get_post_type_object(get_post_type());
                echo $before . $post_type->labels->name . $after;
            } elseif (isset($wp_query->query_vars['taxonomy']) && !empty($wp_query->query_vars['taxonomy'])) {
                $taxonomy = $taxonomy_category = '';
                $taxonomy = $wp_query->query_vars['taxonomy'];
                echo $before . $wp_query->query_vars[$taxonomy] . $after;
            } elseif (is_page() && !$post->post_parent) {
                if ($showCurrent == 1)
                    echo $before . '<a href="' . esc_url( get_permalink() ) . '">' . esc_attr( get_the_title() ) . '</a>' . $after;
            } elseif (is_page() && $post->post_parent) {
                $parent_id = $post->post_parent;
                $breadcrumbs = array();
                while ($parent_id) {
                    $page = get_page($parent_id);
                    $breadcrumbs[] = sprintf($link, esc_url( get_permalink($page->ID) ), esc_attr( get_the_title( $page->ID ) ));
                    $parent_id = $page->post_parent;
                }
                $breadcrumbs = array_reverse($breadcrumbs);
                for ($i = 0; $i < count($breadcrumbs); $i++) {
                    echo $breadcrumbs[$i];
                    if ($i != count($breadcrumbs) - 1)
                        echo $delimiter;
                }

                if ($showCurrent == 1)
                    echo $delimiter . $before . esc_attr( get_the_title() ) . $after;
            } elseif (is_tag()) {
                echo $before . sprintf($text['tag'], esc_attr( single_tag_title( '', FALSE ) ) ) . $after;
            } elseif (is_author()) {
                global $author;
                $userdata = get_userdata($author);
                echo $before . sprintf($text['author'], esc_attr( $userdata->display_name) ) . $after;
            } elseif (is_404()) {
                echo $before . $text['404'] . $after;
            }
            echo '</ul>';
        }
    }
endif;

/**
 * Get Gallery Attachment IDs
 * 
 * @global object $post
 * @return array
 */
function get_gallery_attachments()
{
    global $post;

    $post_content = $post->post_content;
    preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
    $images_id = explode(",", $ids[1]);

    return $images_id;
}

if (!function_exists('neovantage_grab_ids_from_gallery')) :
    
    /**
     * Post gallery
     * 
     * @global object $post
     * @return object
     */
    function neovantage_grab_ids_from_gallery()
    {
        global $post;
        $gallery = neovantage_get_shortcode_from_content('gallery');

        $object = new stdClass();
        $object->columns = '3';
        $object->link = 'post';
        $object->ids = array();
        if($gallery) {
            $object = neovantage_extra_shortcode('gallery', $gallery, $object);
        }
        return $object;
    }
endif;

if ( ! function_exists( 'neovantage_get_shortcode_from_content' ) ) :
    
    /**
     * Get Shortcode From Content
     * 
     * @global object $post
     * @param mixed $param
     * @return string
     */
    function neovantage_get_shortcode_from_content( $param )
    {
        global $post;
        $pattern = get_shortcode_regex();
        
        $content = $post->post_content;
        if ( preg_match_all( '/' . $pattern . '/s', $content, $matches ) && array_key_exists( 2, $matches ) && in_array( $param, $matches[ 2 ] ) ) {
            $key = array_search( $param, $matches[ 2 ] );
            return $matches[ 0 ][ $key ];
        }
    }
endif;

if ( ! function_exists( 'neovantage_extra_shortcode' ) ) :
    
    /**
     * Extra shortcode
     * 
     * @param string $name
     * @param string $shortcode
     * @param object $object
     * @return object
     */
    function neovantage_extra_shortcode( $name, $shortcode, $object )
    {
        if ( $shortcode && is_object( $object ) ) {
            $attrs = str_replace( array ( '[', ']', '"', $name ), null, $shortcode );
            $attrs = explode( ' ', $attrs );
            
            if ( is_array( $attrs ) ) {
                foreach ( $attrs as $attr ) {
                    if( $attr ) {
                        $_attr = explode( '=', $attr );

                        if ( count( $_attr ) == 2 ) {
                            if ( $_attr[0] == 'ids' ) {
                                $object->ids = explode( ',', $_attr[ 1 ] );
                            } elseif ( $_attr[0] == 'columns' ) {
                                $object->columns = $_attr[ 1 ];
                               // $object->$_attr[0] = explode( ',', $_attr[ 1 ] );
                            } else {
                                //$object->$_attr[0] = $_attr[ 1 ];
                            }
                        }
                    }
                }
            }
        }
        return $object;
    }
endif;

if ( ! function_exists( 'neovantage_social_shares' ) ) :
    
    /**
     * Post Social Share Links
     */
    function neovantage_social_shares()
    {
        $page_title = get_the_title();
        $current_url = "http://" . $_SERVER[ 'SERVER_NAME' ] . $_SERVER[ 'REQUEST_URI' ];
?>
    <div class="social-share">
        <span class="social-share-title"><?php echo __( 'Share Post:', 'neovantage' ); ?></span>
        <a href="http://digg.com/submit?url=<?php echo esc_url( $current_url ); ?>&#038;title=<?php echo rawurlencode( $page_title ); ?>" target="_blank" title="<?php echo __('Share on Digg', 'neovantage'); ?>" class="digg">
            <i class="fab fa-digg"></i>
        </a>
        
        <a href="http://www.facebook.com/share.php?u=<?php echo esc_url( $current_url ); ?>" target="_blank" class="facebook">
            <i class="fab fa-facebook-f"></i>
        </a>
        
        <a href="https://plus.google.com/share?url=<?php echo esc_url( $current_url ); ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=500');return false;" class="google-plus">
            <i class="fab fa-google"></i>
        </a>
        
        <a href="http://www.linkedin.com/shareArticle?mini=true&#038;url=<?php echo esc_url( $current_url ); ?>&#038;title=<?php echo rawurlencode( $page_title ); ?>" target="_blank" class="linkedin">
            <i class="fab fa-linkedin-in"></i>
        </a>
        <?php
        $thumbnail_id = get_post_thumbnail_id( get_the_ID() );
        $thumbnail = wp_get_attachment_image_src( $thumbnail_id, 'large' );
        ?>
        <a href="http://pinterest.com/pin/create/button/?url=<?php echo esc_url( $current_url ); ?>&media=<?php echo esc_url( $thumbnail[ 0 ] ); ?>" class="pin-it-button pinterest" count-layout="horizontal" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;">
            <i class="fab fa-pinterest-p"></i>
        </a>	
        
        <a href="http://reddit.com/submit?url=<?php echo esc_url( $current_url ); ?>&#038;title=<?php echo rawurlencode( $page_title ); ?>" target="_blank" class="reddit">
            <i class="fab fa-reddit-alien"></i>
        </a>
        
        <a href="http://www.stumbleupon.com/submit?url=<?php echo esc_url( $current_url ); ?>&#038;title=<?php echo rawurlencode( $page_title ); ?>" target="_blank" class="stumbleupon">
            <i class="fab fa-stumbleupon"></i>
        </a>
        
        <a href="http://twitter.com/home?status=<?php echo str_replace( '%26%23038%3B', '%26', rawurlencode( $page_title ) ) . ' - ' . esc_url( $current_url ); ?>" target="_blank" class="twitter">
            <i class="fab fa-twitter"></i>
        </a>
        <div class="clear"></div>
    </div>
        <?php
    }
endif;


if ( !function_exists( 'neovantage_comment' ) ) :
    
    /**
     * Custom Comments Structure
     * 
     * @param string $comment
     * @param array $args
     * @param int $depth
     */
    function neovantage_comment($comment, $args, $depth)
    {
        $GLOBALS['comment'] = $comment;
        $GLOBALS['depth'] = $depth;
?> 
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>" class="clearfix">
        <div id="comment-<?php comment_ID(); ?>" class="comment-body clearfix">
            <div class="wrapper">
                <div class="comment-author vcard"><?php echo get_avatar( antispambot( $comment->comment_author_email ), 65 ); ?></div>
                <?php if ($comment->comment_approved == '0') : ?>
                    <em><?php _e('Your comment is awaiting moderation.', 'neovantage') ?></em>
                <?php endif; ?>	      	
                <div class="extra-wrap">
                    <?php printf( __('<span class="author">%1$s</span>', 'neovantage' ), get_comment_author_link() ); ?><br />
                    <?php printf( __('%1$s', 'neovantage'), get_comment_date('F j, Y') ); ?>
                    <?php esc_html( comment_text() ); ?>
                </div>
            </div>
            <div class="wrapper">
                <div class="reply">
                    <?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( '<i class="fas fa-reply"></i> Reply', 'neovantage' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                </div>
            </div>
        </div>
    </li>
<?php
    }
endif;

if ( ! function_exists( 'neovantage_site_logo' ) ) :
    
    /**
     * Displays the optional custom logo.
     *
     * Does nothing if the custom logo is not available.
     *
     * @since NEOVANTAGE 1.0.4
     */
    function neovantage_site_logo() {
        if ( function_exists( 'the_custom_logo' ) ) {
            the_custom_logo();
        }
    }
endif;

if( !function_exists('neovantage_get_brightness')) :
    /**
     * Returns brightness value from 0 to 255
     * 
     * @param string $hex
     * @return string
     */
    function neovantage_get_brightness($hex) {
        // Strip off any leading #
        $hex = str_replace('#', '', $hex);

        $c_r = hexdec(substr($hex, 0, 2));
        $c_g = hexdec(substr($hex, 2, 2));
        $c_b = hexdec(substr($hex, 4, 2));

        return (($c_r * 299) + ($c_g * 587) + ($c_b * 114)) / 1000;
    }
endif;

if( !function_exists('neovantage_adjust_brightness')) :
    /**
     * Adjust brightness
     * 
     * @param string $hex
     * @param int $steps
     * @return string
     */
    function neovantage_adjust_brightness($hex, $steps) {
        // Steps should be between -255 and 255. Negative = darker, positive = lighter
        $steps = max(-255, min(255, $steps));

        // Format the hex color string
        $hex = str_replace('#', '', $hex);
        if (strlen($hex) == 3) {
            $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
        }

        // Get decimal values
        $r = hexdec(substr($hex,0,2));
        $g = hexdec(substr($hex,2,2));
        $b = hexdec(substr($hex,4,2));

        // Adjust number of steps and keep it inside 0 to 255
        $r = max(0,min(255,$r + $steps));
        $g = max(0,min(255,$g + $steps));
        $b = max(0,min(255,$b + $steps));

        $r_hex = str_pad(dechex($r), 2, '0', STR_PAD_LEFT);
        $g_hex = str_pad(dechex($g), 2, '0', STR_PAD_LEFT);
        $b_hex = str_pad(dechex($b), 2, '0', STR_PAD_LEFT);

        return '#'.$r_hex.$g_hex.$b_hex;
    }
endif;

/**
 * Get Post Format Icons
 */
function neovantage_get_post_format_icon()
{
    switch (get_post_format()) {
        case 'gallery':
            $post_icon['icon'] = 'fas fa-camera';
            $post_icon['text'] = __('GALLERY', 'neovantage');
            break;
        case 'link':
            $post_icon['icon'] = 'fas fa-link';
            $post_icon['text'] = __('LINK', 'neovantage');
            break;
        case 'quote':
            $post_icon['icon'] = 'fas fa-quote-left';
            $post_icon['text'] = __('QUOTE', 'neovantage');
            break;
        case 'video':
            $post_icon['icon'] = 'fas fa-film';
            $post_icon['text'] = __('VIDEO', 'neovantage');
            break;
        case 'audio':
            $post_icon['icon'] = 'fas fa-music';
            $post_icon['text'] = __('AUDIO', 'neovantage');
            break;
        case 'image':
            $post_icon['icon'] = 'far fa-image';
            $post_icon['text'] = __('IMAGE', 'neovantage');
        break;
        case 'status':
            $post_icon['icon'] = 'far fa-comment';
            $post_icon['text'] = __('Status', 'neovantage');
        break;
        case 'aside':
            $post_icon['icon'] = 'fas fa-align-left';
            $post_icon['text'] = __('Aside', 'neovantage');
        break;
        case 'chat':
            $post_icon['icon'] = 'far fa-comments';
            $post_icon['text'] = __('Chat', 'neovantage');
        break;
        default:
            if(is_sticky()){
                $post_icon['icon'] = 'fas fa-thumbtack';
                $post_icon['text'] = __('STICKY', 'neovantage');
            } else {
                $post_icon['icon'] = 'far fa-file-alt';
                $post_icon['text'] = __('STANDARD', 'neovantage');
            }
            break;
    }
    return $post_icon;
}

/**
 * Get the Featured image HTML of a post
 * 
 * @param integer $post_id
 * @param string $size
 * @param string|array $attr
 * @param bool $archive_only
 * @return string
 */
function neovantage_get_post_thumbnail( $post_id = NULL, $size = 'post-thumbnail', $attr = '', $archive_only = TRUE )
{
    $image_url = '';
  
    if ( has_post_thumbnail( $post_id ) ) {
        $image_url = get_the_post_thumbnail( $post_id, $size, $attr );
    }
    if ( $image_url ) {
        return $image_url;
    } else {
        
        if ( is_single() && TRUE === $archive_only ) {
            return '';
        }
    }
}

/**
 * Render Preloader
 * 
 * @since 1.1.0
 */
function neovantage_render_preloader()
{
    $preloader_status = get_theme_mod( 'preloader', 'enable' );
    if ( isset($preloader_status) && "enable" == $preloader_status ) :
?>
        <!-- It allows to create preloader -->
        <div id="preloader" class="preloader">
            <span><?php _e('Loading', 'neovantage'); ?></span>
        </div>
<?php
    endif;
}