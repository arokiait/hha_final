<?php
/**
 * TGM Init Class
 */
if ( file_exists( dirname( __FILE__ ) . '/class-tgm-plugin-activation.php' ) ) {
    require_once get_template_directory() . '/admin/tgm/class-tgm-plugin-activation.php';
    
    add_action( 'tgmpa_register', 'neovantage_register_plugins' );
    
    /**
     * Register the optional plugins for this theme.
     *
     * In this, we register four plugins:
     * - NEOVANTAGE Core included with the TGMPA library
     * - Redux Framework plugin from the .org repo included with the TGMPA library
     * - NEO Bootstrap Carousel included with the TGMPA library
     * - Contact Form 7 included with the TGMPA library
     * - Instagram Feed included with the TGMPA library
     *
     * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
     */
    function neovantage_register_plugins()
    {
        $plugins = array (
            array (
                'name' => 'NEOVANTAGE CORE',
                'slug' => 'neovantage-core',
                'source' => 'https://downloads.pixelspress.com/plugin/neovantage-core.1.0.7.zip',
                'required' => false,
                'version' => '1.0.7',
                'force_activation' => false,
                'force_deactivation' => false,
                'image_url' => get_stylesheet_directory_uri() . '/images/neovantage-core.jpg',
            ),
            array (
                'name' => 'NEO Bootstrap Carousel',
                'slug' => 'neo-bootstrap-carousel',
                'source' => 'https://downloads.wordpress.org/plugin/neo-bootstrap-carousel.1.3.1.zip',
                'required' => false,
                'version' => '1.3.1',
                'force_activation' => false,
                'force_deactivation' => false,
                'image_url' => get_stylesheet_directory_uri() . '/images/neo-bootstrap-carousel.jpg',
            ),
            array (
                'name' => 'Contact Form 7',
                'slug' => 'contact-form-7',
                'source' => 'https://downloads.wordpress.org/plugin/contact-form-7.5.0.2.zip',
                'required' => false,
                'version' => '5.0.2',
                'force_activation' => false,
                'force_deactivation' => false,
                'image_url' => get_stylesheet_directory_uri() . '/images/contact-form-7.jpg',
            ),
            array (
                'name' => 'Instagram Feed',
                'slug' => 'instagram-feed',
                'source' => 'https://downloads.wordpress.org/plugin/instagram-feed.1.8.3.zip',
                'required' => false,
                'version' => '1.8.3',
                'force_activation' => false,
                'force_deactivation' => false,
                'image_url' => get_stylesheet_directory_uri() . '/images/instagram-feed.jpg',
            ),
        );
        
        $config = array (
            'default_path' => '', // Default absolute path to pre-packaged plugins
            'menu' => 'neovantage-install-plugins', // Menu slug
            'has_notices' => true, // If true, admin notices are shown for required/recommended plugins.
            'dismissable' => true, // If true, admin admin notices can be dismissed by the user.
            'dismiss_msg' => '', // If 'dismissable' is false, this message will be output at top of nag.
            'is_automatic' => false, // If true, plugins will automatically be activated upon successful installation (for both singular and bulk installation processes).
            'message' => '', // Optional HTML content to include before the plugins table is output. This string will be filtered by wp_kses_post().
        );
        tgmpa( $plugins, $config );
    }
}