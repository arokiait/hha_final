--- 
customlog: 
  - 
    format: combined
    target: /etc/apache2/logs/domlogs/hemmenassoc.com
  - 
    format: "\"%{%s}t %I .\\n%{%s}t %O .\""
    target: /etc/apache2/logs/domlogs/hemmenassoc.com-bytes_log
documentroot: /home/hemmenassoc/public_html
group: hemmenassoc
hascgi: 0
homedir: /home/hemmenassoc
ifmodulemodpassengerc: 
  passengergroup: 
    - 
      value: " hemmenassoc"
  passengeruser: 
    - 
      value: " hemmenassoc"
ip: 68.66.197.32
owner: newvps
phpopenbasedirprotect: 1
phpversion: ea-php70
port: 80
scriptalias: 
  - 
    path: /home/hemmenassoc/public_html/cgi-bin
    url: /cgi-bin/
serveradmin: webmaster@hemmenassoc.com
serveralias: mail.hemmenassoc.com www.hemmenassoc.com
servername: hemmenassoc.com
usecanonicalname: 'Off'
user: hemmenassoc
userdirprotect: ''
